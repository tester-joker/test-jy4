# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_litemall.py
# date:2022/7/24
import time
import allure
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from log.demo_logger import logger


# 自定义显示等待条件
# 第一个参数是定位方法，第二个参数是定位的语句，第三个参使是最大点击次数，这里设置了五次
def click_exception(by, element, max_attempts=5):
    def _inner(driver):
        # 多次点击按钮
        # 实际点击次数
        actul_attempts = 0
        # 如果实际点击次数超过最大点击次数，抛出异常
        while actul_attempts < max_attempts:
            actul_attempts += 1
            try:
                driver.find_element(by, element).click()
                return True
            except Exception:
                logger.debug("点击时出现了一次异常")
        raise Exception("超出了最大点击次数")

    return _inner


class TestLitemall:
    def get_screen(self):
        timestamp = int(time.time())
        image_path = f"./images/image_{timestamp}.PNG"
        # 将截图放在报告中
        self.driver.save_screenshot(image_path)
        allure.attach.file(image_path, name="picture",
                           attachment_type=allure.attachment_type.PNG)

    def setup_class(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(3)
        self.driver.maximize_window()
        self.driver.get("https://litemall.hogwarts.ceshiren.com/")
        self.driver.find_element(By.NAME, "username").clear()
        self.driver.find_element(By.NAME, "username").send_keys("manage")
        self.driver.find_element(By.NAME, "password").clear()
        self.driver.find_element(By.NAME, "password").send_keys("manage123")
        self.driver.find_element(By.CSS_SELECTOR, ".el-button").click()

    def teardown_class(self):
        self.driver.quit()

    def test_add_type(self):
        self.driver.find_element(By.XPATH, "//*[text()='商场管理']").click()
        self.driver.find_element(By.XPATH, "//*[text()='商品类目']").click()
        self.driver.find_element(By.CSS_SELECTOR, ".el-icon-edit").click()
        self.driver.find_element(By.CSS_SELECTOR, ".el-input__inner").send_keys("add_test_joker")

        WebDriverWait(self.driver, 10).until(
            click_exception(By.CSS_SELECTOR, ".dialog-footer>button:nth-child(2)")
        )
        # find_elements可以找到多个元素，并返回一个列表，如果没找到，则返回空列表
        res = self.driver.find_elements(By.XPATH, "//*[text()='add_test_joker']")
        self.get_screen()
        self.driver.find_element(By.XPATH, "//*[text()='add_test_joker']/../..//span[text()='删除']").click()
        logger.info(f"获取到的实际结果为{res}")
        assert res != []
        # 用例产生了脏数据，需要进行请理，可以使用ui的方式，也可以使用接口的方式
        # 数据请理要放在断言操作之后操作，否则可能会影响断言结果

    def test_delete_type(self):
        self.driver.find_element(By.XPATH, "//*[text()='商场管理']").click()
        self.driver.find_element(By.XPATH, "//*[text()='商品类目']").click()
        self.driver.find_element(By.CSS_SELECTOR, ".el-icon-edit").click()
        self.driver.find_element(By.CSS_SELECTOR, ".el-input__inner").send_keys("add_test_joker")
        self.driver.find_element(By.CSS_SELECTOR, ".dialog-footer>button:nth-child(2)").click()
        # 删除商品
        WebDriverWait(self.driver, 10).until(
            click_exception(By.XPATH, "//*[text()='add_test_joker']/../..//span[text()='删除']")
        )
        # self.driver.find_element(By.XPATH,"//*[text()='add_test_joker']/../..//span[text()='删除']").click()
        # 因为代码执行速度过快，元素还未消失就去捕获了
        max_time = 10
        res = WebDriverWait(self.driver, max_time).until(
            # 在查找的列表中有任意一个找到的话，就会返回True
            visibility_element_located(By.XPATH, "//*[text()='add_test_joker']", max_time)
        )
        logger.info(f"获取到的实际结果为{res}")
        assert res == True


def visibility_element_located(by, element, maxtime):
    def _inner(driver):
        a_time = 0
        while a_time < maxtime:
            a_time += 0.5
            try:
                driver.find_element(by, element)
                return True
            except Exception:
                logger.debug("查找信息时发生了一个异常")
        return False

    return _inner
