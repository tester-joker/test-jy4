# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_litemall_PO.py
# date:2022/7/29

import pytest
from test_litemall.page_objects.login_page import LoginPage


class TestLitemall:

    def setup_class(self):
        """登录页面：用户登录"""
        self.home = LoginPage().login()

    def teardown_class(self):
        self.home.do_quit()

    @pytest.mark.parametrize("category_name",
                             ["joker", "add_joker"])
    def test_add_type(self, category_name):
        """登录页面：用户登录"""
        list_page = self.home \
            .go_to_category() \
            .click_add() \
            .create_category(category_name)
        # 断言
        res = list_page.get_operate_result()
        assert "创建成功" == res
        # 数据请理
        list_page.delete_category(category_name)
        """系统首页：进入商品类目"""
        """类目列表页面：点击添加"""
        """创建类目页面：创建类目"""
        """类目列表页面：获取操作结果"""

    @pytest.mark.parametrize("category_name",
                             ["joker", "add_joker"])
    def test_delete_type(self, category_name):
        res = self.home \
            .go_to_category() \
            .click_add() \
            .create_category(category_name) \
            .delete_category(category_name) \
            .get_delete_result()
        assert "删除成功" == res

# def visibility_element_located(by, element,maxtime):
#     def _inner(driver):
#         a_time = 0
#         while a_time < maxtime:
#             a_time += 0.5
#             try:
#                 driver.find_element(by, element)
#                 return True
#             except Exception:
#                 logger.debug("查找信息时发生了一个异常")
#         return False
#     return _inner
