# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:web_util.py
# date:2022/7/29
from test_litemall.utils.log_utils import logger


def click_exception(by, element, max_attempts=5):
    def _inner(driver):
        # 多次点击按钮
        # 实际点击次数
        actul_attempts = 0
        # 如果实际点击次数超过最大点击次数，抛出异常
        while actul_attempts < max_attempts:
            actul_attempts += 1
            try:
                driver.find_element(by, element).click()
                return True
            except Exception:
                logger.debug("点击时出现了一次异常")
        raise Exception("超出了最大点击次数")

    return _inner
