# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:category_creat_page.py
# date:2022/7/29
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from test_litemall.page_objects.base_page import BasePage
from test_litemall.utils.log_utils import logger
from test_litemall.utils.web_util import click_exception


class CategoryCreatPage(BasePage):
    __INPUT_NAME = (By.CSS_SELECTOR, ".el-input__inner")
    __BTN_CONFIRM = (By.CSS_SELECTOR, ".dialog-footer>button:nth-child(2)")

    """创建类目页面：创建类目"""
    def create_category(self,category_name:str):
        logger.info("创建类目页面：创建类目")
        # 输入“类目名称”
        self.do_send_keys(category_name,self.__INPUT_NAME)
        # 点击“确定”按钮
        WebDriverWait(self.driver, 10).until(
            click_exception(*self.__BTN_CONFIRM)
        )
        # ==》类目列表页面
        from test_litemall.page_objects.category_list_page import CategoryListPage
        return CategoryListPage(self.driver)