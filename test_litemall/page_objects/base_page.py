# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:base_page.py
# date:2022/7/29
# 定义父类，把一些公用的方法封装在父类中，让其他的类继承该类
import time
import allure
from selenium import webdriver


class BasePage:
    _BASE_URL = ""

    def __init__(self, base_driver=None):
        if base_driver:
            self.driver = base_driver
        else:
            self.driver = webdriver.Chrome()
            self.driver.implicitly_wait(3)
            self.driver.maximize_window()
        if not self.driver.current_url.startswith("http"):
            self.driver.get(self._BASE_URL)

    def do_find(self, by, locator=None):
        # 获取单个元素
        if locator:
            return self.driver.find_element(by, locator)
        else:
            return self.driver.find_element(*by)

    def do_finds(self, by, locator=None):
        # 获取多个元素
        if locator:
            return self.driver.find_elements(by, locator)
        else:
            return self.driver.find_elements(*by)

    def do_send_keys(self, value, by, locator=None):
        ele = self.do_find(by, locator)
        ele.clear()
        ele.send_keys(value)

    def do_quit(self):
        self.driver.quit()

    def get_screen(self):
        timestamp = int(time.time())
        image_path = f"./images/image_{timestamp}.PNG"
        # 将截图放在报告中
        self.driver.save_screenshot(image_path)
        allure.attach.file(image_path, name="picture",
                           attachment_type=allure.attachment_type.PNG)
