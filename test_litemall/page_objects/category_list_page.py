# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:category_list_page.py
# date:2022/7/29
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from test_litemall.page_objects.base_page import BasePage
from test_litemall.utils.log_utils import logger

class CategoryListPage(BasePage):
    __ADD = (By.CSS_SELECTOR, ".el-icon-edit")

    """类目列表页面：点击添加"""
    def click_add(self):
        logger.info("类目列表页面：添加类目")
        # 点击“添加”按钮
        self.do_find(self.__ADD).click()
        # ==》创建类目页面
        from test_litemall.page_objects.category_creat_page import CategoryCreatPage
        return CategoryCreatPage(self.driver)


    """类目列表页面：获取操作结果"""
    def get_operate_result(self):
        logger.info("类目列表页面：获取创建操作结果")
        # 获取冒泡消息文本
        element = WebDriverWait(self.driver,10).until\
            (expected_conditions.visibility_of_element_located
             ((By.XPATH,'//p[contains(text(),"创建")]')))
        msg = element.text
        logger.info(f"{msg}")
        # ==》返回消息文本
        return msg


    def delete_category(self,category_name):
        logger.info("类目列表页面：删除操作")
        # 对指定的类目进行删除,这里需要参数化，所以不能封装属性
        self.do_find(By.XPATH,f"//*[text()='{category_name}']/../..//span[text()='删除']").click()
        # ==> 跳转到当前页
        return CategoryListPage(self.driver)


    def get_delete_result(self):
        logger.info("类目列表页面：获取删除操作结果")
        # 获取冒泡消息文本
        element = WebDriverWait(self.driver,10).until\
            (expected_conditions.visibility_of_element_located
             ((By.XPATH,'//p[contains(text(),"删除")]')))
        msg = element.text
        logger.info(f"冒泡信息为{msg}")
        # ==》返回消息文本
        return msg