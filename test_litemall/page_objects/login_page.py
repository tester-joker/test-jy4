# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:login_page.py
# date:2022/7/29

from selenium.webdriver.common.by import By
from test_litemall.page_objects.base_page import BasePage
from test_litemall.utils.log_utils import logger


class LoginPage(BasePage):
    _BASE_URL = "https://litemall.hogwarts.ceshiren.com/"
    # 封装定位属性，这样的好处就是，如果页面元素发生了变化，只需要更改属性就可以了
    __INPUT_USERNAME = (By.NAME, "username")
    __INPUT_PASSWORD = (By.NAME, "password")
    __TO_LOGIN = (By.CSS_SELECTOR, ".el-button")

    """登录页面：用户登录"""
    def login(self):
        # 访问登录页
        logger.info("登录页面：用户登录")
        # 输入“用户名”
        self.do_send_keys("manage",self.__INPUT_USERNAME)
        # 输入“密码”
        self.do_send_keys("manage123",self.__INPUT_PASSWORD)
        # 点击“登录”按钮
        self.do_find(self.__TO_LOGIN).click()
        # ==》首页

        from test_litemall.page_objects.home_page import HomePage
        return HomePage(self.driver)

