# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:demo3.py
# date:2022/9/25

# class B:
#     def list_and_delete(self, name):
#         good = self.list(name)
#         self.delete(good)
#
#
# class A(B):
#     def list(self, name):
#         return name
#
#     def delete(self, id):
#         print(id)
#
#
# demo = A()
# demo.list_and_delete('name')

class Demo:
    demo = "开发"

    def __init__(self):
        self.demo1 = "测试"

    def hogwarts(self):
        print(f"这是一个{self.demo1}方法")

    @classmethod
    def age(cls):
        print(f"这是类方法里的{cls.demo}方法")
        cls.age2()

    @classmethod
    def age2(cls):
        print("这是第二个类方法")

    @staticmethod
    def name():
        print("这是一个静态方法")


s = Demo()
Demo.hogwarts(s)
s.hogwarts()

Demo.age()
s.age()

Demo.name()
s.name()

