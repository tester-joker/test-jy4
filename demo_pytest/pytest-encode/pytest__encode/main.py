# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:main.py
# date:2022/10/3
from typing import List


# 修改编码的hook函数
def pytest_collection_modifyitems(
        session: "Session", config: "Config", items: List["Item"]
) -> None:
    # items里的name是测试用例的名字，nodeid是测试用例的路径
    print(items)
    for item in items:
        # 如果想改变unicode编码格式的话，需要先encode成utf-8格式的，再decode成unicode-escape就可以了
        item.name = item.name.encode('utf-8').decode('unicode-escape')
        item._nodeid = item.nodeid.encode('utf-8').decode('unicode-escape')
