# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:setup.py
# date:2022/10/3
from setuptools import setup, find_packages

setup(
    name='pytest_encode',
    url='',
    version='1.0',  # 版本
    author='joker',  # 作者
    author_email='',  # 邮箱
    description='set your encoding and logger',  # 描述用法
    long_description='Show Chinese for you mark.parametrize().',  # 完整描述
    classifiers=[  # 分类索引，pip所属包的分类，方便在pip官网中搜索
        'Framework :: Pytest',
        'Programming Language :: Python',
        'Topic :: Software Development :: Testing',
        'Programming Language :: Python :: 3.8',

    ],
    license='proprietary',  # 程序授权信息
    packages=find_packages(),  # 通过导入的方式发现当前项目下所有的包
    keywords=[  # 便于pip进行分类
        'pytest', 'py.test', 'pytest_encode'
    ],
    # 需要安装的依赖
    install_requires=[
        'pytest'
    ],
    # 入口模块，或者入口函数(最重要的)通过pytest11参数找到主模块
    entry_points={
        'pytest11': [
            'pytest_encode = pytest_encode.main'
        ]
    },
    zip_safe=False
    # 针对win系统，不设置成false会出错
)