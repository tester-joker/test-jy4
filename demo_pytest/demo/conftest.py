# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:conftest.py
# date:2022/9/29
from typing import List

import pytest


# 修改编码的hook函数
def pytest_collection_modifyitems(
        session: "Session", config: "Config", items: List["Item"]
) -> None:
    # items里的name是测试用例的名字，nodeid是测试用例的路径
    print(items)
    for item in items:
        # 如果想改变unicode编码格式的话，需要先encode成utf-8格式的，再decode成unicode-escape就可以了
        item.name = item.name.encode('utf-8').decode('unicode-escape')
        item._nodeid = item.nodeid.encode('utf-8').decode('unicode-escape')


# 定义命令行参数的hook函数
def pytest_addoption(parser):
    # group 将下面所有的option都展示在这个group组下
    mygroup = parser.getgroup('hogwarts')
    mygroup.addoption('--env',  # 注册一个命令行选项
                      default='test',  # 参数的默认值
                      dest='env',  # 存储的变量，为属性命令，可以使用option对象访问到这个值
                      help='set your run env')  # 帮助提示，参数的描述信息


@pytest.fixture(scope='session')
def cmd_option(request):
    # request获取命令行的参数，config拿到pytest相关配置，getoption拿到命令行参数
    return request.config.getoption('--env')
