# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_pytest_demo.py
# date:2022/9/7

import pytest


@pytest.fixture()
def login():
    # setup
    token = '1235236fdg'
    print("登录功能")
    yield token  # 相当于return 返回none
    # teardown
    print("退出登录操作")


# def test_search(demo, login):
#     print("搜索功能")


def test_cart(login):
    print(f"token:{login}")
    print("购物车")


@pytest.fixture(params=['hogwarts', 'joker'])
def demo_params(request):
    print(f'用户名为:{request.param}')
    print(type(request.param))
    return request.param


def test_demo(demo_params):
    print(f"数据为:{demo_params}")


if __name__ == '__main__':
    demo_params()
