# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_litemall.py
# date:2022/9/14
import json
import pytest
import requests

from interface_litemall.log_utils import logger


class TestLitemall:
    # 自动获取token信息
    def setup_class(self):
        # 管理端登录接口
        url = 'https://litemall.hogwarts.ceshiren.com/admin/auth/login'
        admin_login = {"username":"admin123","password":"admin123","code":""}
        r = requests.post(url,json=admin_login)
        self.admin_token = r.json()['data']['token']
        # 用户端登录接口
        url = 'https://litemall.hogwarts.ceshiren.com/wx/auth/login'
        user_login = {"username": "user123", "password": "user123"}
        r = requests.post(url, json=user_login)
        self.user_token = r.json()['data']['token']

    # 数据请理,直接调用delete接口删除数据
    def teardown(self):
        del_url = 'https://litemall.hogwarts.ceshiren.com/admin/goods/delete'
        de = requests.post(url=del_url,json={'id': self.goods_id},headers={"X-Litemall-Admin-Token": self.admin_token})
        logger.info(f'删除商品接口的响应信息为：{json.dumps(de.json(), indent=2, ensure_ascii=False)}')


    @pytest.mark.parametrize('goods_name',['joker','joker123'])
    def test_add_goods(self,goods_name):
        # 上架商品接口
        url = 'https://litemall.hogwarts.ceshiren.com/admin/goods/create'
        goods_data = {
            "goods": {"picUrl": "", "gallery": [], "isHot": False, "isNew": True, "isOnSale": True, "goodsSn": "10800",
                      "name": goods_name}, "specifications": [{"specification": "规格", "value": "标准", "picUrl": ""}],
            "products": [{"id": 0, "specifications": ["标准"], "price": "88", "number": "10", "url": ""}],
            "attributes": []}
        re = requests.post(url,json=goods_data,headers={"X-Litemall-Admin-Token": self.admin_token})
        logger.debug(f'上架商品接口的响应信息为：{json.dumps(re.json(),indent=2,ensure_ascii=False)}')

        # 获取商品列表
        goods_list_url = 'https://litemall.hogwarts.ceshiren.com/admin/goods/list'
        goods_data = {
            'name': goods_name,
            'order': 'desc',
            'sort': 'add_time'
        }
        re = requests.get(url=goods_list_url, params=goods_data,
                          headers={"X-Litemall-Admin-Token": self.admin_token})
        self.goods_id = re.json()['data']['list'][0]['id']
        logger.debug(f'获取商品列表的响应信息为：{json.dumps(re.json(), indent=2, ensure_ascii=False)}')
        #===============================
        # 获取商品详情接口
        goods_detail_url = 'https://litemall.hogwarts.ceshiren.com/admin/goods/detail'
        re = requests.get(url=goods_detail_url, params={"id": self.goods_id},
                          headers={"X-Litemall-Admin-Token": self.admin_token})
        productId = re.json()['data']['products'][0]['id']
        logger.debug(f'获取商品详情接口的响应信息为：{json.dumps(re.json(), indent=2, ensure_ascii=False)}')
        # 添加购物车
        url = 'https://litemall.hogwarts.ceshiren.com/wx/cart/add'
        cart_data = {"goodsId": self.goods_id,"number":1,"productId":productId}
        r = requests.post(url, json=cart_data,headers={"X-Litemall-Token": self.user_token})
        res = r.json()
        logger.info(f'添加购物车接口的响应信息为：{json.dumps(r.json(), indent=2, ensure_ascii=False)}')

        # 添加数据库断言
        from interface_litemall.connect import execute_sql
        sql_res = execute_sql(f"select * from litemall_cart where user_id=1 and deleted=0 and "
                              f"goods_name='{goods_name}'")
        assert sql_res
        assert res['errmsg'] == '成功'


