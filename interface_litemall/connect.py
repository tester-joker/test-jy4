# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:connect.py
# date:2022/9/15
import pymysql

# 连接数据库
def get_connect():
    conn = pymysql.connect(
        host='litemall.hogwarts.ceshiren.com',
        port=13306,
        user='test',
        password='test123456',
        database='litemall',
        charset='utf8mb4'
    )
    return conn


# 执行sql语句
def execute_sql(sql):
    connect = get_connect()
    cursor = connect.cursor()
    cursor.execute(sql)  # 执行sql语句
    record = cursor.fetchone()  # 获取一条结果信息
    return record


if __name__ == '__main__':
    execute_sql('select * from litemall_cart')