# -*- coding: UTF-8 -*-
class Animal:
    def __init__(self,name,color,age,sex):
        self.name = name
        self.color = color
        self.age = age
        self.sex = sex
    def run(self,name):
        print(f"{name}会跑")
    def call(self,name):
        print(f"{name}会叫")


class Cat(Animal):
    def __init__(self,name,color,age,sex,hair):
        super().__init__(name,color,age,sex)
        self.hair=hair

    def ability(self):
        return "会抓老鼠"

    def call(self):
        return "喵喵叫"


class Dog(Animal):
    def __init__(self,name,color,age,sex,hair):
        super().__init__(name,color,age,sex)
        self.hair = hair
    def ability(self):
        return "会看家"

    def call(self):
        return "汪汪叫"


if __name__=='__main__':
    cat = Cat("小猫猫","黄色",5,"男","短毛")
    dog = Dog("小狗狗","白黑色",3,"女","长毛")
    for animal in (cat,dog):
        print(f"姓名为{animal.name},颜色:{animal.color},年龄:{animal.age},性别:{animal.sex},毛发为{animal.hair}的动物，它的特长是{animal.ability()},它{animal.call()}")
