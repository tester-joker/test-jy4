# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_cart.py
# date:2022/9/24
'''
框架优化：
1. 先把接口和用例步骤写出来，接口的实现暂时先为空
2. 初步的实现接口，保证可用
'''
import allure
import pytest

from Interface_litemall_po.apis.admin.goods import Goods
from Interface_litemall_po.apis.wechat.cart import Cart
from Interface_litemall_po.utils.log_utils import logger


class TestCart:
    def setup_class(self):
        self.goods = Goods('https://litemall.hogwarts.ceshiren.com', 'admin')
        self.cart = Cart('https://litemall.hogwarts.ceshiren.com', 'client')

    @allure.story("添加购物车测试用例")
    @pytest.mark.parametrize('goods_name', ['joker123', 'joker1234'])
    def test_add_cart(self, goods_name):
        '''
        添加购物车的测试用例：
            上架商品接口
            获取商品列表
            获取商品详情接口
            添加购物车
        '''
        with allure.step('上架商品'):
            goods_data = {
                "goods": {"picUrl": "", "gallery": [], "isHot": False, "isNew": True, "isOnSale": True, "goodsSn": "10800",
                          "name": goods_name}, "specifications": [{"specification": "规格", "value": "标准", "picUrl": ""}],
                "products": [{"id": 0, "specifications": ["标准"], "price": "88", "number": "10", "url": ""}],
                "attributes": []}
            self.goods.create(goods_data)
        with allure.step('获取商品列表，拿到列表第一个商品的id'):
            goods_list = self.goods.list(goods_name)
            goods_list_id = goods_list['data']['list'][0]['id']
            logger.debug(f"获取到的goods_id为{goods_list_id}")
        with allure.step('根据商品id，获取商品详情'):
            goods_detail = self.goods.detail(goods_list_id)
            productId = goods_detail['data']['products'][0]['id']
            logger.debug(f"获取到的productId为{productId}")
        with allure.step('添加购物车'):
            result = self.cart.add(goods_id=goods_list_id, product_id=productId)

        with allure.step('数据请理'):
            '''
            放在这里的目的：
            1. 调用方法使用方便
            2. 并不是所有的测试方法都会添加goods数据，
            如果部分用例不添加数据，那如果将数据请理放在teardown里的话，会出问题
            '''
            self.goods.delete(goods_id=goods_list_id)
            # self.goods.delete_by_name(goods_name)
        assert result['errmsg'] == '成功'
