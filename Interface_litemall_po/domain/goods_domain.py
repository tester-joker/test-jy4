# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:goods_domain.py
# date:2022/9/25

'''
domain：业务层的抽象
本意是业务的抽象，涉及设计模式，架构等相关知识
'''


# 不去实例化父类

class GoodsDomain:
    '''
    抽象的概念，不做具体的实现
    代表的是某一种业务的模型
    '''

    def delete_by_name(self, name):
        goods_list = self.list(name)
        goods_list_id = goods_list['data']['list'][0]['id']
        self.delete(goods_list_id)


'''
这里的逻辑举例说明：
A子类继承了B父类，子类中一个list，一个delete方法，父类中有一个delete_by_name方法
实例化子类对象去调用父类的delete_by_name方法，父类的方法又调用了子类的list和delete方法
虽然父类中代码会报警告，但是可以正常执行
'''