# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:base_api.py
# date:2022/9/24
import json

import requests

from Interface_litemall_po.utils.log_utils import logger


class BaseApi:
    '''
    问题：大量重复的base_url
    解决方案：在构造函数中，实例化base_url,可以在send方法中去拼接base_url，也可以在goods类中调用base_url的实例
    优化：token的获取也放在构造函数中
    '''

    def __init__(self, base_url, role=None):
        self.base_url = base_url
        # 获取对应角色的信息
        if role:
            self.role = role

    '''
    问题：接口里面直接使用了requests，可维护性差，添加日志也需要一个一个添加
    解决：在baseapi中添加公共的send方法
    '''

    # 封装请求以及日志
    def send(self, method, url, **kwargs):
        kwargs = self.__set_token(kwargs)
        r = requests.request(method, url, **kwargs)
        # 如果``ensure_ascii``为false，则返回值可以包含非ascii字符
        # 如果``indent``是非负整数，则JSON数组元素和对象成员将以该缩进级别打印出来。0是最紧凑的
        logger.debug(f"{url}接口响应值为{json.dumps(r.json(), indent=2, ensure_ascii=False)}")
        return r.json()

    # 因为获取token都是在base层使用，所以可以封装成私有方法
    def __set_token(self, request_info):
        '''
        问题 1：除登录接口外，每个接口都需要去设置token
        解决方案：
        1. 在发起接口请求前就获取token信息
        2. 获取后 塞入到请求信息中
        cart_api ->send -> 获取kwargs -> 在kwargs中塞入header等（鉴权）信息

        问题 2：有两个不同的token怎么办
        解决方案：不同的角色用不同的token
        :return:
        '''
        # 管理端登录接口
        admin_url = f'{self.base_url}/admin/auth/login'
        admin_login = {"username": "hogwarts", "password": "test12345", "code": ""}
        admin_r = requests.request('post', admin_url, json=admin_login)
        self.admin_token = {"X-Litemall-Admin-Token": admin_r.json()['data']['token']}

        # 客户端登录接口
        client_url = f'{self.base_url}/wx/auth/login'
        client_login = {"username": "user123", "password": "user123"}
        client_r = requests.request('post', client_url, json=client_login)
        self.client_token = {"X-Litemall-Token": client_r.json()['data']['token']}

        # 判断角色信息，分别给对应的token信息
        if self.role == 'admin':
            self.token = self.admin_token
        else:
            self.token = self.client_token

        # 获取headers,有头信息就将token信息更新进去，没有headers就创建headers
        # 注意，如果有头信息一定要更新，而不是将headers替换，那样的话头信息会被覆盖
        if request_info.get('headers'):
            request_info['headers'].update(self.token)
        else:
            request_info['headers'] = self.token
        return request_info
