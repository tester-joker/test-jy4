# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:cart.py
# date:2022/9/24
from Interface_litemall_po.apis.base_api import BaseApi


class Cart(BaseApi):
    def add(self, goods_id, product_id):
        url = f'{self.base_url}/wx/cart/add'
        cart_data = {"goodsId": goods_id, "number": 1, "productId": product_id}
        r = self.send('post', url, json=cart_data)
        return r
