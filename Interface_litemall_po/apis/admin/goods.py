# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:goods.py
# date:2022/9/24
import json

from Interface_litemall_po.apis.base_api import BaseApi
from Interface_litemall_po.domain.goods_domain import GoodsDomain
from Interface_litemall_po.utils.log_utils import logger


class Goods(BaseApi, GoodsDomain):

    def create(self, goods_data):
        url = f'{self.base_url}/admin/goods/create'
        re = self.send('post', url, json=goods_data)
        return re

    def list(self, goods_name, order='desc', sort='add_time'):
        # 自己编写接口的对应方法，应该与接口本身逻辑相关
        goods_list_url = f'{self.base_url}/admin/goods/list'
        goods_data = {
            'name': goods_name,
            'order': order,
            'sort': sort
        }
        re = self.send('get', url=goods_list_url, params=goods_data)
        return re

    def detail(self, goods_id):
        goods_detail_url = f'{self.base_url}/admin/goods/detail'
        re = self.send('get', url=goods_detail_url, params={"id": goods_id})
        return re

    def delete(self, goods_id):
        del_url = f'{self.base_url}/admin/goods/delete'
        de = self.send('post', url=del_url, json={'id': goods_id})
        logger.info(f'删除商品接口的响应信息为：{json.dumps(de, indent=2, ensure_ascii=False)}')
        return de
