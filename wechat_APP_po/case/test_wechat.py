# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_wechat.py
# date:2022/8/21
from faker import Faker

from wechat_APP_po.base.wechat import WeChat
from wechat_APP_po.utils.genrate_info import GenerateInfo


class TestWechat():
    def setup_class(self):
        self.app = WeChat()

    def setup(self):
        self.main = self.app.start().goto_main()

    def teardown(self):
        self.app.stop()


    def test_case(self):
        name = GenerateInfo.get_name()
        phone = GenerateInfo.get_phone()
        tips = self.main.mail_list()\
            .add_member("添加成员")\
            .search_add()\
            .fill_info(name,phone)\
            .get_result()
        assert "添加成功" == tips

    def test_case2(self):
        name = GenerateInfo.get_name()
        phone = GenerateInfo.get_phone()
        tips = self.main.mail_list() \
            .add_member("添加成员") \
            .search_add() \
            .fill_info(name, phone) \
            .get_result()
        assert "添加成功" == tips