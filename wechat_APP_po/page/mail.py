# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:mail.py
# date:2022/8/21
from appium.webdriver.common.mobileby import MobileBy
from wechat_APP_po.base.base_page import BasePage
from wechat_APP_po.page.add_member import AddMember


class Mail(BasePage):
    _MAILLIST = (MobileBy.XPATH, "//*[@text='通讯录']")
    def mail_list(self):
        self.find_and_click(*self._MAILLIST)
        return AddMember(self.driver)