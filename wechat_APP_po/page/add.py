# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:add.py
# date:2022/8/21
from appium.webdriver.common.mobileby import MobileBy

from wechat_APP_po.base.base_page import BasePage


class SearchAdd(BasePage):
    _SEARCHADD = (MobileBy.XPATH, "//*[@text='手动输入添加']")
    _RESULT = (MobileBy.XPATH, "//*[@class='android.widget.Toast']")
    def search_add(self):
        self.find_and_click(*self._SEARCHADD)
        from wechat_APP_po.page.edit_member import EditMember
        return EditMember(self.driver)

    def get_result(self):
        return self.driver.find_element(*self._RESULT).text


