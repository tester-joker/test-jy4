# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:edit_member.py
# date:2022/8/21
from appium.webdriver.common.mobileby import MobileBy

from wechat_APP_po.base.base_page import BasePage

class EditMember(BasePage):
    _SEARCHNAME = (MobileBy.XPATH, "//*[contains(@text,'姓名')]/../*[@text='必填']")
    _SEARCHPHONE = (MobileBy.XPATH, "//*[contains(@text,'手机')]/..//*[@text='必填']")
    _ADD = (MobileBy.XPATH, "//*[@text='保存']")

    def fill_info(self,name,phonenumber):
        # python不支持循环导入，即a导入了B，B导入了A,所以这里变成局部导入就可以了
        from wechat_APP_po.page.add import SearchAdd
        self.find_and_send(*self._SEARCHNAME, name)
        self.find_and_send(*self._SEARCHPHONE, phonenumber)
        self.find_and_click(*self._ADD)
        return SearchAdd(self.driver)