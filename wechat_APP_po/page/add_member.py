# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:add_member.py
# date:2022/8/21
from appium.webdriver.common.mobileby import MobileBy

from wechat_APP_po.base.base_page import BasePage
from wechat_APP_po.page.add import SearchAdd


class AddMember(BasePage):
    _IMPLICITLY = 20
    def add_member(self,text):
        _FIND = (MobileBy.XPATH, f"//*[@text='{text}']")
        self.swipe_find(_FIND,self._IMPLICITLY).click()
        return SearchAdd(self.driver)