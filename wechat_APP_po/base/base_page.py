# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:base_page.py
# date:2022/8/21

# 基本方法：driver，find等
import logging
import time

import allure
import cv2
from appium.webdriver.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException

class BasePage:
    # 注意：导包应该是appium下的WebDriver，不是selenium的
    def __init__(self, driver: WebDriver=None):
        self.driver = driver

    def find(self,by,locator):
        logging.info(f"查找元素：{locator}")
        element = self.driver.find_element(by,locator)
        imgpath = self.draw_rectangle_in_screenshot(element)
        allure.attach.file(imgpath, name="页面截图", attachment_type=allure.attachment_type.PNG)
        return element

    def find_and_click(self,by,locator):
        self.find(by,locator).click()

    def find_and_send(self,by,locator,text):
        self.find(by,locator).send_keys(text)

    def swipe_find(self,ele, time,max_num=3):
        self.set_implicit(1)
        for i in range(max_num):
            try:
                element = self.find(*ele)
                self.set_implicit(time)
                return element
            except NoSuchElementException:
                # 获取当前手机尺寸
                logging.info("滑动页面")
                size = self.driver.get_window_size()
                startx = size.get("width") / 2
                starty = size.get("height") * 0.8

                endx = startx
                endy = size.get("height") * 0.3
                # swipe滑动操作,duration滑动时间
                self.driver.swipe(startx, starty, endx, endy, duration=500)
            if i == max_num - 1:
                # 如果超过查找最大次数，抛出异常
                logging.error("超过最大查找次数")
                self.set_implicit(time)
                raise NoSuchElementException(f"找了{max_num}次，没找到")

    # 切换等待时间方法，由于每次查找都会显示等待，所以滑动查找的时候，需要临时将等待时长缩短
    def set_implicit(self,mwaittime):
        return self.driver.implicitly_wait(mwaittime)

    def screenshot(self,name):
        logging.info("截图")
        self.driver.save_screenshot(name)

    # 封装方法，将定位到的元素用矩形框起来，显示在截图中
    # 注意：不可以用在定位toask元素中，因为toask存在时间短，会触发异常：
    # selenium.common.exceptions.StaleElementReferenceException: Message: androidx.test.uiautomator.StaleObjectException
    # 这种异常，通常是指这个元素虽然当是定位到了，但是dom结构发生了更新
    def draw_rectangle_in_screenshot(self, element, color_rgb=(255, 0, 0)):
        '''在图上上画矩形
        start_point: 起点的坐标，tuple 类型，例如：(100, 100)
        end_point: 终点的坐标，tuple 类型，例如：(200, 200)
        color_rgb: 画线对应的rgb颜色 例如：(0, 255, 0)
        '''
        start_x = element.location.get("x")
        start_y = element.location.get("y")
        end_x = element.size.get("width") + start_x
        end_y = element.size.get("height") + start_y
        start_point = (start_x, start_y)
        end_point = (end_x, end_y)
        img_path = "tmp.png"
        self.screenshot(img_path)

        # 读取图片
        image = cv2.imread(img_path)
        # 画矩形
        cv2.rectangle(image, start_point, end_point, color_rgb, 5)
        # 写到文件中
        cv2.imwrite(img_path, image)
        return img_path


    def get_time(self):
        # 获取当前时间
        t = time.localtime(time.time())
        cur_time = time.strftime("%Y-%m-%d_%H_%M_%S", t)
        print(f"当前时间为:{cur_time}")
        return cur_time

