# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:wechat.py
# date:2022/8/21
import logging

from appium import webdriver

from wechat_APP_po.base.base_page import BasePage



class WeChat(BasePage):
    _IMPLICITLY = 20
    def start(self):
        if self.driver == None:
            logging.info("初始化driver")
            desired_caps = {}
            desired_caps['platformName'] = 'Android'
            desired_caps['platformVersion'] = '6.0'
            desired_caps['deviceName'] = '127.0.0.1:7555'
            desired_caps['appPackage'] = 'com.tencent.wework'
            desired_caps['appActivity'] = '.launch.LaunchSplashActivity'
            desired_caps['noReset'] = 'true'
            # 第一种设置方式 ：等待页面处于idle空闲状态 默认是10s,
            desired_caps['settings[waitForIdleTimeout]'] = 1000
            # 不重启应用，需要搭配noreset使用
            desired_caps['dontStopAppOnReset'] = 'true'
            # 跳过安装服务
            desired_caps['skipServerInstallation'] = True
            # 跳过设备的初始化
            desired_caps['skipDeviceInitialization'] = 'true'
            self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
            # 每次find_element的时候都会显示等待，一般来说，这种显示等待已经够了，所以不需要再去设置等待
            self.set_implicit(self._IMPLICITLY)
        else:
            # 直接启动app
            logging.info("复用driver")
            # 启动appActivity页面
            self.driver.launch_app()
        return self

    def stop(self):
        logging.info("关闭app")
        self.driver.quit()

    def goto_main(self):
        from wechat_APP_po.page.mail import Mail
        return Mail(self.driver)