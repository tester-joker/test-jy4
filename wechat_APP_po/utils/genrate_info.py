# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:genrate_info.py
# date:2022/8/21
from faker import Faker

class GenerateInfo:
    @classmethod
    def get_name(self):
        return Faker('zh_CN').name()
    @classmethod
    def get_phone(self):
        return Faker('zh_CN').phone_number()