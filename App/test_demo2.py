# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_demo2.py
# date:2022/8/11
from appium import webdriver
from appium.webdriver.extensions.android.gsm import GsmCallActions

class TestApi():
    def setup(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        # desired_caps['platformVersion'] = '6.0'
        desired_caps['deviceName'] = 'emulator-5554'
        desired_caps['appPackage'] = 'com.xueqiu.android'
        desired_caps['appActivity'] = '.view.WelcomeActivityAlias'
        desired_caps['noReset'] = 'true'

        # desired_caps['skipServerInstallation'] = True
        # 启用unicode输入， Unicode是ASCII(美国信息交换标准码)字符编码的一个扩展。
        desired_caps['unicodeKeyBoard'] = 'true'
        # 在使用Unicode键盘功能运行Unicode测试后，将键盘重置为其原始状态。如果单独使用，则忽略。
        desired_caps['resetKeyBoard'] = 'true'
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
        self.driver.implicitly_wait(10)

    def teardown(self):
        self.driver.quit()

    def test_mobile(self):
        self.driver.make_gsm_call('13012341234',GsmCallActions.CALL)
        self.driver.send_sms('13012341234','hello appium api')