# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_browser.py
# date:2022/8/19
from time import sleep
from appium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

class TestDemo:
    def setup(self):
        caps = {}
        caps['platformName'] = 'android'
        caps['platformVersions'] = '6.0'
        # 要自动化的移动 Web 浏览器的名称。如果改为自动化应用程序，则应为空字符串。
        # 适用于 iOS 的“Safari”和 Android 的“Chrome”、“Chromium”或“浏览器”
        caps['browserName'] = 'Chrome'
        caps['noReset'] = True
        caps['deviceName'] = '127.0.0.1:7555'
        # 指定chromedriver下载地址
        caps['chromedriverExecutableDir'] = 'D:\\Driver'
        self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)
        self.driver.implicitly_wait(10)

    def test_webview(self):
        self.driver.get("https://m.baidu.com")

        self.driver.find_element_by_id("index-kw").send_keys("appium")
        search_locator = (By.ID,"index-bn")
        # 检查元素是否存在于对象的DOM上的期望页面和可见。
        WebDriverWait(self.driver,10).until(expected_conditions.visibility_of_element_located(search_locator))
        self.driver.find_element(*search_locator).click()
        sleep(5)

    def teardown(self):
        # pass
        self.driver.quit()