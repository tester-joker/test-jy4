# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:temo.py
# date:2022/8/3
# This sample code uses the Appium python client
# pip install Appium-Python-Client
# Then you can paste this into a file and simply run with Python
import logging
from time import sleep
from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy

from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class TestAppDemo:
    def setup(self):
        # 创建一个字典，disirecapbility
        caps = {}
        caps["ensureWebviewsHavePages"] = True
        caps["platformName"] = "android"
        # caps["noReset"] = "true"
        """当涉及到登录时，如果不设置这个参数，每次执行测试用例的时候，都会先清除掉缓存信息（包含登录信息）
        如果设置了该参数，那么提前录入的登录信息就不会被清除"""

        # 创建driver，与appium server建立连接,返回一个session对象
        self.driver = webdriver.Remote("http://localhost:4723/wd/hub", caps)
        self.driver.implicitly_wait(5)

    def teardown(self):
        self.driver.quit()

    def test_input(self):
        el1 = self.driver.find_element(MobileBy.XPATH,"//*[@text = 'API Demos'] ")
        el1.click()
        sleep(2)
        el2 = self.driver.find_element(MobileBy.ACCESSIBILITY_ID,"OS")
        el2.click()
        el3 = self.driver.find_element(MobileBy.ACCESSIBILITY_ID,"Morse Code")
        el3.click()
        el4 = self.driver.find_element(MobileBy.ID,"io.appium.android.apis:id/text")
        el4.send_keys("ceshiren.com")
        self.driver.implicitly_wait(3)
        self.driver.back()
        self.driver.back()
        self.driver.back()
        res = self.driver.find_element(MobileBy.ACCESSIBILITY_ID,"Accessibility").text
        assert res == "Accessibility"


    def test_seeking(self):
        """
        1.打开 demo.apk
        2.点击 Animation 进入下个页面
        3.点击 Seeking 进入下个页面
        4.查看【RUN】按钮是否显示/是否可点击
        5.查看【滑动条】是否显示/是否可用/是否可点击
        6.获取【滑动条】长度
        7.点击【滑动条】中心位置
        :return:
        """
        self.driver.find_element(MobileBy.ACCESSIBILITY_ID,"Animation").click()
        self.driver.find_element(MobileBy.ACCESSIBILITY_ID,"Seeking").click()
        run_element = self.driver.find_element(MobileBy.ACCESSIBILITY_ID,"Run")
        # 4.查看【RUN】按钮是否显示/是否可点击
        run_is_displayed = run_element.is_displayed()
        run_is_clickable = run_element.get_attribute("clickable")
        print(f"【RUN】按钮是否可见：{run_is_displayed},是否可点击:{run_is_clickable}")
        # 5.查看【滑动条】是否显示/是否可用/是否可点击
        seek_element = self.driver.find_element(MobileBy.ID,"io.appium.android.apis:id/seekBar")
        seek_displayed = seek_element.is_displayed()
        seek_enabled = seek_element.is_enabled()
        seek_clickable = seek_element.get_attribute("clickable")
        print(f"【滑动条】是否显示:{seek_displayed},是否可用:{seek_enabled},是否可点击:{seek_clickable}")
        # 6.获取【滑动条】长度
        seek_size = seek_element.size
        width = seek_size.get("width")
        height = seek_size.get("height")
        print(f"seek 长度为：{width}")
        # 7.点击【滑动条】中心位置
        seek_location = seek_element.location
        x = seek_location.get("x")
        y = seek_location.get("y")
        self.driver.tap([(x+width/2,y)])
        sleep(5)

"""
    打开【雪球】应用首页
    点击搜索框（点击之前，判断搜索框的是否可用,并查看搜索框 name 属性值，并获取搜索框坐标，以及它的宽高）
    向搜索框输入:alibaba
    判断【阿里巴巴】是否可见
        如果可见，打印“搜索成功”
        如果不可见，打印“搜索失败
"""
class TestXueQiu:
    def setup(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '6.0'
        desired_caps['deviceName'] = '127.0.0.1:7555'
        desired_caps['appPackage'] = 'com.xueqiu.android'
        desired_caps['appActivity'] = '.view.WelcomeActivityAlias'
        desired_caps['noReset'] = 'true'

        desired_caps['skipServerInstallation'] = True
        desired_caps['unicodeKeyBoard']='true'
        desired_caps['resetKeyBoard'] = 'true'
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
        self.driver.implicitly_wait(10)

    def teardown(self):
        self.driver.quit()

    def test_search(self):
        logging.info("搜索用例")
        search_ele = self.driver.find_element(MobileBy.ID,"com.xueqiu.android:id/tv_search")
        search_enable = search_ele.is_enabled()
        if search_enable == True:
            logging.info("点击搜索框")

            search_text = search_ele.text
            search_location = search_ele.location
            search_size = search_ele.size
            print(f"搜索框name属性值:{search_text},搜索框的坐标：{search_location},搜索框的宽高：{search_size}")
            # 点击搜索框
            search_ele.click()
            self.driver.implicitly_wait(5)

            logging.info("向输入框中输入内容")
            # 搜索框中输入alibaba
            self.driver.find_element(MobileBy.ID,"com.xueqiu.android:id/search_input_text").send_keys("alibaba")
            # 判断阿里巴巴是否可见
            alibaba = self.driver.find_elements(MobileBy.XPATH,"//*[@text='阿里巴巴']")[1]
            result = alibaba.get_attribute("displayed")

            logging.info("结果是否处于显示状态："+result)
            logging.info("搜索结果页面源码为："+self.driver.page_source)
            self.driver.save_screenshot("./image/result.png")
            assert "true" == result
        else:
            print("搜索框不可用")


    def test_wait(self):
        self.driver.find_element(MobileBy.ID,"com.xueqiu.android:id/tv_search").click()
        self.driver.find_element(MobileBy.ID,
                                     "com.xueqiu.android:id/search_input_text").send_keys("alibaba")
        self.driver.find_element(MobileBy.XPATH, "//*[@text='阿里巴巴']").click()
        self.driver.find_element(MobileBy.XPATH,"//*[contains(@resource-id,'title_container')]//*[@text='股票']").click()
        locator = (MobileBy.XPATH,"//*[@text='09988']/../../..//*[@resource-id='com.xueqiu.android:id/current_price']")
        # 1
        # WebDriverWait(self.driver,10).until(
        #     expected_conditions.element_to_be_clickable(locator))
        # ele = self.driver.find_element(*locator)
        # 2
        ele = WebDriverWait(self.driver,10).until(lambda x:x.find_element(*locator))
        # ele = self.driver.find_element(MobileBy.XPATH,
        #                          "//*[@text='09988']/../../..//*[@resource-id='com.xueqiu.android:id/current_price']")
        print(ele.text)
        current_price = float(ele.text)
        assert current_price < 100


    def test_touchaction(self):
        from appium.webdriver.common.touch_action import TouchAction
        action = TouchAction(self.driver)
        window_rect = self.driver.get_window_rect() # 获取当前窗口的x,y轴信息。高度以及宽度
        width = window_rect['width']
        height = window_rect['height']
        x = int(width/2)
        y_end = int(height*1/5)
        y_start = int(height*3/5)
        # 完成从页面的五分之三处滑动到五分之一处，即向上滑动
        # press点击     wait设置等待时长，单位mm  move_to滑动的最终位置  release释放  perform执行
        action.press(x=x,y=y_start).wait(2000).move_to(x=x,y=y_end).release().perform()


class TestTouchAction():
    def setup(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '6.0'
        desired_caps['deviceName'] = '127.0.0.1:7555'
        desired_caps['appPackage'] = 'cn.kmob.screenfingermovelock'
        desired_caps['appActivity'] = 'com.samsung.ui.MainActivity'
        desired_caps['noReset'] = 'true'

        desired_caps['skipServerInstallation'] = True
        desired_caps['unicodeKeyBoard'] = 'true'
        desired_caps['resetKeyBoard'] = 'true'
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
        self.driver.implicitly_wait(10)

    def teardown(self):
        self.driver.quit()

    def test_touchaction_unlock(self):
        from appium.webdriver.common.touch_action import TouchAction
        action = TouchAction(self.driver)
        action.press(x=121,y=177).wait(200)\
            .move_to(x=371,y=178).wait(200)\
            .move_to(x=593,y=162).wait(200)\
            .move_to(x=604,y=418).wait(200)\
            .move_to(x=599,y=659).release().perform()



