# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_wechat2.py
# date:2022/8/21

from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
from selenium.common.exceptions import NoSuchElementException

class TestWechat:
    _IMPLICITLY = 20

    def setup(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '6.0'
        desired_caps['deviceName'] = '127.0.0.1:7555'
        desired_caps['appPackage'] = 'com.tencent.wework'
        desired_caps['appActivity'] = '.launch.LaunchSplashActivity'
        desired_caps['noReset'] = 'true'
        # 第一种设置方式 ：等待页面处于idle空闲状态 默认是10s
        desired_caps['settings[waitForIdleTimeout]'] = 1000
        # 不重启应用
        desired_caps['dontStopAppOnReset'] = 'true'
        # 跳过安装服务
        desired_caps['skipServerInstallation'] = True
        # 跳过设备的初始化
        desired_caps['skipDeviceInitialization'] = 'true'
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
        # 每次find_element的时候都会显示等待，一般来说，这种显示等待已经够了，所以不需要再去设置等待
        self.driver.implicitly_wait(self._IMPLICITLY)

    def teardown(self):
        self.driver.quit()

    # 切换等待时间方法，由于每次查找都会隐示等待，所以滑动查找的时候，需要临时将等待时长缩短
    def set_implicit(self, waittime):
        self.driver.implicitly_wait(waittime)

    def swipe_find(self,text,max_num = 3):
        self.set_implicit(1)
        for i in range(max_num):
            try:
                element = self.driver.find_element(MobileBy.XPATH,f"//*[@text='{text}']")
                self.set_implicit(self._IMPLICITLY)
                return element
            except NoSuchElementException:
                # 获取当前手机尺寸
                size = self.driver.get_window_size()
                startx = size.get("width")/2
                starty = size.get("height")*0.8

                endx = startx
                endy = size.get("height")*0.3
                # swipe滑动操作,duration滑动时间，时间不能过长，他会先去点击，然后再滑动，如果时间太长会出现长按的问题
                self.driver.swipe(startx,starty,endx,endy,duration=500)
            if i == max_num-1:
                # 如果超过查找最大次数，抛出异常
                self.set_implicit(self._IMPLICITLY)
                raise NoSuchElementException(f"找了{max_num}次，没找到")


    def test_clock(self):
        self.driver.find_element(MobileBy.XPATH,"//*[@text='工作台']").click()
        self.swipe_find('打卡').click()
        # 第二种方式 ，设置页面等待页面处于idle（空闲）状态的时长
        # self.driver.update_settings({"waitForIdleTimeout": 0})
        self.driver.find_element(MobileBy.XPATH,"//*[@text='外出打卡']").click()
        self.driver.find_element(MobileBy.XPATH,"//*[contains(@text,'次外出')]").click()
        result = self.driver.find_element(MobileBy.ID,"com.tencent.wework:id/rr").text
        assert "外出打卡成功" == result