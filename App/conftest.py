# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:conftest.py
# date:2022/8/11
import time
import pytest

@pytest.fixture(scope="session", autouse=True)
def manage_logs(request):
    """Set log file name same as test name"""
    now = time.strftime("%Y-%m-%d %H-%M-%S")
    log_name = 'log/' + now + '.logs'

    request.config.pluginmanager.get_plugin("logging-plugin") \
        .set_log_path(log_name)