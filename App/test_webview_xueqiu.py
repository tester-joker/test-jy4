# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_webview_xueqiu.py
# date:2022/8/20

from time import sleep

from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy


class TestDemo:
    def setup(self):
        caps = {}
        caps["appPackage"] = "com.xueqiu.android"
        caps["appActivity"] = "com.xueqiu.android.common.WebViewActivity"
        caps["platformName"] = "android"
        caps["ensureWebviewsHavePages"] = True
        caps['chromedriverExecutableDir'] = 'D:\\Driver'
        caps['showChromedriverLog'] = True

        self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)

    def test_webview(self):
        self.driver.find_element(MobileBy.XPATH,"//*[@text='交易']").click()


    def teardown(self):
        pass
        # self.driver.quit()