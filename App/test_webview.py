# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_webview.py
# date:2022/8/19
from time import sleep

from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy


class TestDemo:
    def setup(self):
        caps = {}
        caps["appPackage"] = "io.appium.android.apis"
        caps["appActivity"] = ".ApiDemos"
        caps["platformName"] = "android"
        caps["ensureWebviewsHavePages"] = True
        caps['chromedriverExecutableDir'] = 'D:\\Driver'
        caps['showChromedriverLog'] = True

        self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)

    def test_webview(self):
        self.driver.find_element_by_accessibility_id("Views").click()
        self.driver.find_element(
            MobileBy.ANDROID_UIAUTOMATOR,
            'new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().text("WebView").instance(0));'
        ).click()

        for i in range(5):
            contexts = self.driver.contexts
            print(contexts)
            if len(contexts) > 1:
                break
            sleep(1)

        self.driver.switch_to.context('WEBVIEW_io.appium.android.apis')
        self.driver.find_element(MobileBy.NAME, 'i_am_a_textbox').send_keys("123")
        self.driver.find_element(MobileBy.LINK_TEXT, 'i am a link').click()
        print(self.driver.page_source)

    def teardown(self):
        # pass
        self.driver.quit()