# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_wechat.py
# date:2022/8/14
import time
from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
from faker import Faker
from selenium.common.exceptions import NoSuchElementException


class TestWechat:
    _IMPLICITLY = 20
    def setup_class(self):
        testfaker = Faker("zh_Cn")
        self.name = testfaker.name()
        self.phone = testfaker.phone_number()

    def setup(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '6.0'
        desired_caps['deviceName'] = '127.0.0.1:7555'
        desired_caps['appPackage'] = 'com.tencent.wework'
        desired_caps['appActivity'] = '.launch.LaunchSplashActivity'
        desired_caps['noReset'] = 'true'
        # 不重启应用
        desired_caps['dontStopAppOnReset'] = 'true'
        # 跳过安装服务
        desired_caps['skipServerInstallation'] = True
        # 跳过设备的初始化
        desired_caps['skipDeviceInitialization'] = 'true'
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
        # 每次find_element的时候都会显示等待，一般来说，这种显示等待已经够了，所以不需要再去设置等待
        self.driver.implicitly_wait(self._IMPLICITLY)

    def teardown(self):
        self.driver.quit()

    # 切换等待时间方法，由于每次查找都会显示等待，所以滑动查找的时候，需要临时将等待时长缩短
    def set_implicit(self, waittime):
        self.driver.implicitly_wait(waittime)

    def swipe_find(self,text,max_num = 3):
        self.set_implicit(1)
        for i in range(max_num):
            try:
                element = self.driver.find_element(MobileBy.XPATH,f"//*[@text='{text}']")
                self.set_implicit(self._IMPLICITLY)
                return element
            except NoSuchElementException:
                # 获取当前手机尺寸
                size = self.driver.get_window_size()
                startx = size.get("width")/2
                starty = size.get("height")*0.8

                endx = startx
                endy = size.get("height")*0.3
                # swipe滑动操作,duration滑动时间
                self.driver.swipe(startx,starty,endx,endy,duration=500)
            if i == max_num-1:
                # 如果超过查找最大次数，抛出异常
                self.set_implicit(self._IMPLICITLY)
                raise NoSuchElementException(f"找了{max_num}次，没找到")

    def test_case(self):
        self.driver.find_element(MobileBy.XPATH,"//*[@text='通讯录']").click()
        # self.driver.find_element(MobileBy.XPATH,"//*[@text='添加成员']").click()
        self.swipe_find('添加成员').click()
        self.driver.find_element(MobileBy.XPATH,"//*[@text='手动输入添加']").click()
        # 由于可能text中包含空格，所以可以通过contains去定位包含某些文本的元素
        self.driver.find_element(MobileBy.XPATH,"//*[contains(@text,'姓名')]/../*[@text='必填']").send_keys(self.name)
        self.driver.find_element(MobileBy.XPATH,"//*[contains(@text,'手机')]/..//*[@text='必填']").send_keys(self.phone)
        self.driver.find_element(MobileBy.XPATH,"//*[@text='保存']").click()
        # time.sleep(4)
        # print(self.driver.page_source)
        toast = self.driver.find_element(MobileBy.XPATH, "//*[@class='android.widget.Toast']").text
        assert "添加成功" == toast
