# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_html.py
# date:2022/9/24
import time

import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By


class TestDemo:

    def setup_class(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()

    # @pytest.mark.parametrize("name,number,money,lit",
    #                          [('张三','12345678901','1000','100'),
    #                           ('李四','12435678901','1000','100'),
    #                           ('王五','32536576856','1000','100'),
    #                           ('赵六','23515465743','1000','100'),
    #                           ('小七','23545463267','1000','100')])
    def test_demo1(self):
        name = '张三'
        number = 12345678901
        money = 1000
        lit = 100
        self.driver.get("http://192.168.137.1:8080/main.html")
        self.driver.implicitly_wait(3)

        self.driver.find_element(By.XPATH, '//*[text()="客户管理"]').click()
        frame = self.driver.find_element(By.CSS_SELECTOR, "iframe")
        self.driver.switch_to.frame(frame)
        self.driver.find_element(By.XPATH, '//span[text()="添加客户"]').click()
        self.driver.find_element(By.CSS_SELECTOR, '.el-dialog__wrapper .el-input__inner').send_keys(name)
        self.driver.find_element(By.XPATH, '//span[text()="男"]/..//span[@class="el-radio__input"]').click()
        self.driver.find_element(By.CSS_SELECTOR, ".el-input>input").send_keys(number)
        time.sleep(3)

    def teardown_class(self):
        self.driver.quit()
