# -*- coding: UTF-8 -*-
"""
实战
某群有多个成员，群主给成员发普通红包。

发红包的规则是：
1、群主负责发红包，不能抢红包。红包金额从群主余额中扣除，按成员人数平均分成n等份，以备领取。
每个红包的金额为整数，如果有余数则加到最后一个红包中。
2、成员负责抢红包。抢到的红包金额存到自己余额中。
3、抢完红包后需要进行报数，打印格式“我是XX，现在有 XX 块钱。”。

请根据描述信息，完成案例中所有类的定义、类之间的继承关系，以及发红包、抢红包的操作。
"""
import random
class Person:
    def __init__(self,name,money):
        self.money = money
        self.name = name
    def show(self):
        print(f"我是{self.name},我有{self.money}元")

class Manager(Person):
    def send(self,money,num):
        # 群主发红包，扣除群主余额的钱
        self.money -= money
        # 红包列表
        red_list = []
        a = money//num
        for i in range(num):
            red_list.append(a)
        b = money%num
        red_list[-1]+=b
        return red_list


class Member(Person):
    def grab(self,red_list):
        # 创建随机数，从红包列表的下标里随机一个，然后pop掉说明发出去了
        index = random.randint(0,len(red_list) - 1)
        m = red_list.pop(index)
        self.money+=m
        return self.money


if __name__ == '__main__':
    s1 = Manager('张三',100)
    s2 = Member('成员1',0)
    s3 = Member('成员2',0)
    s4 = Member('成员3',0)
    n = 0
    for i in s3,s2,s4,s1:
        i.show()
        n+=1
    print("="*10 + "发红包"+ "="*10)
    list = s1.send(s1.money,n-1)
    print("="*10 + "收红包"+ "="*10)
    for i in s2,s3,s4:
        i.grab(list)
    print("="*10 + "结果"+ "="*10)
    for i in s3,s2,s4,s1:
        i.show()
