# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_add.py
# date:2022/7/10

# 测试计算器相加的方法
import allure
import pytest
from pytest_base.test.bases.base import Base
from pytest_base.test.utils.log_util import logger


@allure.feature("计算器相加测试设计")
class TestAdd(Base):

    def teardown_class(self):
        print("结束测试")

    @pytest.mark.P0
    @allure.title("正向验证")
    @allure.story("P0级别用例")
    @pytest.mark.parametrize('a,b,expect',
                             [(1,1,2),
                              (-0.01,0.02,0.01),
                              (10,0.02,10.02)])
    def test_add1(self,a,b,expect):
        logger.info(f"输入数据：{a},{b},预期结果{expect}")
        with allure.step("步骤1：两数相加"):
            result = self.cal.add(a,b)
        logger.info(f"实际结果：{result}")
        with allure.step("步骤2：断言"):
            assert expect == result

    @pytest.mark.P1
    @allure.title("有效边界值验证")
    @allure.story("P1级别用例")
    @pytest.mark.parametrize('a,b,expect',
                             [(98.99,99,197.99),
                              (99,98.99,197.99),
                              (-98.99,-99,-197.99),
                              (-99,-98.99,-197.99)
                              ])
    def test_add2(self,a,b,expect):
        logger.info(f"输入数据：{a},{b},预期结果{expect}")
        with allure.step("步骤1：两数相加"):
            result = self.cal.add(a,b)
        logger.info(f"实际结果：{result}")
        with allure.step("步骤2：断言"):
            assert expect == result

    @pytest.mark.P1
    @allure.title("无效边界值验证")
    @allure.story("P1级别异常用例")
    @pytest.mark.parametrize('a,b,expect',
                             [(99.01,0,"参数大小超出范围"),
                              (-99.01,-1,"参数大小超出范围"),
                              (2,99.01,"参数大小超出范围"),
                              (1,-99.01,"参数大小超出范围")
                              ])
    def test_add3(self,a,b,expect):
        logger.info(f"输入数据：{a},{b},预期结果{expect}")
        with allure.step("步骤1：两数相加"):
            result = self.cal.add(a,b)
        logger.info(f"实际结果：{result}")
        with allure.step("步骤2：断言"):
            assert expect == result

    @pytest.mark.P1
    @allure.title("中文验证")
    @allure.story("P1级别中文异常用例")
    @pytest.mark.parametrize('a,b,expect',
                             [('文',9.3,"请输入范围为【-99, 99】的整数或浮点数"),
                              (4,'字',"请输入范围为【-99, 99】的整数或浮点数")
                              ])
    def test_add4(self,a,b,expect):
        # 捕获异常方法一
        try:
            logger.info(f"输入数据：{a},{b},预期结果{expect}")
            with allure.step("步骤1：两数相加"):
                result = self.cal.add(a,b)
        except TypeError:
            print(expect)
        else:
            logger.info(f"实际结果：{result}")
            with allure.step("步骤2：断言"):
                assert expect == result

        #捕获异常方法二
        # with pytest.raises(TypeError) as e:
        #     result = self.cal.add(a, b)
        #     print(e)
        #     assert expect == result
