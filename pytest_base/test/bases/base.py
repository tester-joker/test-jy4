# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:base.py
# date:2022/7/10
from pytest_base.script.calculator import Calculator
from pytest_base.test.utils.log_util import logger


class Base:
    def setup_class(self):
        logger.info("实例化计算器对象")
        self.cal = Calculator()

    def setup(self):
        print("开始计算")

    def teardown(self):
        print("结束计算")