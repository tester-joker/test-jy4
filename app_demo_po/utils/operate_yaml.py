# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:operate_yaml.py
# date:2022/8/16

import logging
import yaml

# 读取yaml文件
def get_data(filename):
    logging.info("获取数据")
    with open(filename) as f:
        datas = yaml.safe_load(f)
        return datas