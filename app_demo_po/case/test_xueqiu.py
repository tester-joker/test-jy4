# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_xueqiu.py
# date:2022/8/13
import pytest
from hamcrest import assert_that, close_to

from app_demo_po.base.xueqiu_app import XueqiuApp
from app_demo_po.utils import operate_yaml


class TestXueQiu:
    search_datas = operate_yaml.get_data("../datas/searchdata.yml")
    def setup_class(self):
        # 启动app
        self.xueqiuapp = XueqiuApp()

    def teardown_class(self):
        self.xueqiuapp.stop()

    def setup(self):
        self.main = self.xueqiuapp.start().goto_main()

    @pytest.mark.parametrize("search_key,search_result,price",search_datas)
    def test_search(self,search_key,search_result,price):
        """
        打开【雪球】应用首页
        点击搜索框，进入搜素页面
        输入【alibaba】
        切换到tab的【股票】
        找到股票【阿里巴巴】的股票价格 price
        判断price 在100上下百分之10浮动
        :return:
        """
        stock_price = self.main.click_search().\
            input_search(search_key).\
            click_search_result(search_result).\
            goto_stock_tab().get_price(search_result)

        # hamcrest
        # 如果对象是在给定增量内接近给定值的数字，则匹配。
        # 第一个参数是要比较的值，是一个预期值
        # 第二个参数是指定数值之间最大的差值，如果高于或低于这个差值，就不会匹配
        # close_to方法将计算对象与“值”进行比较，查看差值是否在增量内
        # 接近某个值的范围
        assert_that(stock_price,close_to(price,0.1*price))

    def test_search1(self):
        stock_price = self.main.click_search().\
            input_search("alibaba").\
            click_search_result().\
            goto_stock_tab().get_price()
        assert_that(stock_price,close_to(100,0.1*100))