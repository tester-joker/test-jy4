# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:main_page.py
# date:2022/8/13
import time
from appium.webdriver.common.mobileby import MobileBy
from app_demo_po.base.xueqiu_app import XueqiuApp
from app_demo_po.page.search_page import SearchPage


class MainPage(XueqiuApp):
    _SEARCH_ELEMENT = (MobileBy.ID, "com.xueqiu.android:id/tv_search")

    def click_search(self):
        # 弹窗异常处理：手动制造弹窗
        self.find_and_click(MobileBy.ID, "com.xueqiu.android:id/post_status")
        # 首页，点击搜索框
        self.find_and_click(*self._SEARCH_ELEMENT)
        return SearchPage(self.driver)
