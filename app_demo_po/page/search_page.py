# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:search_page.py
# date:2022/8/13
from appium.webdriver.common.mobileby import MobileBy
from app_demo_po.base.xueqiu_app import XueqiuApp
from app_demo_po.page.search_result_page import SearchResultPage


class SearchPage(XueqiuApp):
    _SEARCH_SEND_KEYS = (MobileBy.ID, "com.xueqiu.android:id/search_input_text")

    def input_search(self, searchKey):
        # 搜索框输入页面
        self.find_and_send(*self._SEARCH_SEND_KEYS, searchKey)
        return self

    def click_search_result(self, result):
        self.find_and_click(MobileBy.XPATH, f"//*[@text='{result}']")
        return SearchResultPage(self.driver)
