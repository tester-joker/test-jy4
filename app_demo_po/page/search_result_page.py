# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:search_result_page.py
# date:2022/8/13
from appium.webdriver.common.mobileby import MobileBy
from app_demo_po.base.xueqiu_app import XueqiuApp

class SearchResultPage(XueqiuApp):
    _RESULT_STOCK_TAB = (MobileBy.XPATH,"//*[@text='股票']")
    def goto_stock_tab(self):
        # 点击股票
        self.find_and_click(*self._RESULT_STOCK_TAB)
        return self

    def get_price(self,result):
        # 找到阿里巴巴对应的股票价格
        current_price = self.find(MobileBy.XPATH,
                                 f"//*[@text='{result}']/../../..//*[@resource-id='com.xueqiu.android:id/current_price']").text
        return float(current_price)