# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:base_page.py
# date:2022/8/13

# 基本方法：driver，find等
import time

from appium.webdriver.webdriver import WebDriver
from app_demo_po.base.black_handle import black_wrapper


class BasePage:
    def __init__(self, driver: WebDriver = None):
        self.driver = driver

    def find(self, by, locator):
        return self.driver.find_element(by, locator)

    @black_wrapper
    def find_and_click(self, by, locator):
        self.driver.find_element(by, locator).click()

    def find_and_send(self, by, locator, text):
        self.find(by, locator).send_keys(text)

    def screenshot(self, name):
        # 截图
        self.driver.save_screenshot(name)

    def get_time(self):
        # 获取当前时间
        t = time.localtime(time.time())
        cur_time = time.strftime("%Y-%m-%d_%H_%M_%S", t)
        print(f"当前时间为:{cur_time}")
        return cur_time
