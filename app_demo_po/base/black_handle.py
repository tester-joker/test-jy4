# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:black_handle.py
# date:2022/8/15
# 弹窗黑名单，将弹窗异常处理封装成装饰器
# 被装饰方法在执行前会将方法传入装饰器，先去执行装饰器
import logging
import os
import traceback

import allure
from appium.webdriver.common.mobileby import MobileBy

black_list = [(MobileBy.ID, "com.xueqiu.android:id/iv_close")]


def black_wrapper(fun):
    # fun是被装饰的方法，在执行被装饰方法前会将方法作为对象传入装饰器内部执行逻辑，然后再去执行被装饰方法
    def run(*args, **kwargs):
        from app_demo_po.base.base_page import BasePage
        basepage: BasePage = args[0]  # 接受self对象，被装饰方法的第一个参数是self，下面需要用到实例self去定位
        try:
            logging.info(f"开始查找元素:{args[2]}")
            # 定位无异常，返回fun，继续执行被装饰方法
            return fun(*args, **kwargs)
        except Exception as e:
            logging.warning("未找到元素，抓取截图")
            # 截图如果不指定路径的话， 会自动保存到执行脚本的路径，即case目录下
            tmp_name = basepage.get_time() + ".png"
            logging.info("当前保存图片路径 >>>" + os.path.dirname(__file__))
            # 拼接路径，  os.path.driname是当前路径（即base文件夹）,并拼接图片名称
            tmp_path = os.path.join(os.path.dirname(__file__), "..", "images", tmp_name)
            basepage.screenshot(tmp_path)
            allure.attach.file(tmp_path, name="弹窗截图", attachment_type=allure.attachment_type.PNG)
            for black in black_list:
                logging.info(f"处理黑名单：{black}")
                # 将黑名单的元素解包并去定位，如果能找到就去点击，如果找不到就继续遍历，直到遍历整个黑名单列表，抛出异常
                ele = basepage.driver.find_element(*black)
                if ele:  # 如果不为空，则证明定位到了元素，去点击并返回fun
                    logging.info(f"点击黑名单弹框，退出")
                    ele.click()
                    return fun(*args, **kwargs)
            logging.error(f"遍历黑名单后，未找到元素，异常信息 ====>{e}")
            logging.error(f"命令行的内容 ====>{traceback.format_exc()}")
            raise e

    return run
