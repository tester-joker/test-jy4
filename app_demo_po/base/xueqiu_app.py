# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:xueqiu_app.py
# date:2022/8/13
from appium import webdriver
from app_demo_po.base.base_page import BasePage


# app 相关的操作：start,stop,restart
class XueqiuApp(BasePage):
    def start(self):
        # 如果执行多条测试用例时，每次都去初始化driver浪费资源
        # 解决方案：在创建self.driver的时候加一个判断，如果driver为none的话，则创建，否则复用dirver
        if self.driver == None:
            print("dirver == None")
            desired_caps = {}
            desired_caps['platformName'] = 'Android'
            desired_caps['platformVersion'] = '6.0'
            desired_caps['deviceName'] = '127.0.0.1'
            desired_caps['appPackage'] = 'com.xueqiu.android'
            desired_caps['appActivity'] = '.view.WelcomeActivityAlias'
            desired_caps['noReset'] = 'true'

            self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
            self.driver.implicitly_wait(10)
        else:
            # 直接启动app
            print("复用driver")
            self.driver.launch_app()
        return self

    def restart(self):
        pass

    def stop(self):
        self.driver.quit()

    def goto_main(self):
        from app_demo_po.page.main_page import MainPage
        return MainPage(self.driver)
