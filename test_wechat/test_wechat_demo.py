# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_wechat.py
# date:2022/7/24

# https://work.weixin.qq.com/wework_admin/loginpage_wx
import time
import yaml
from faker import Faker
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class TestWechat:
    def setup_class(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(3)
        self.driver.maximize_window()

    def teardown_class(self):
        self.driver.quit()

    def test_get_cookie(self):
        self.driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx")
        time.sleep(10)
        cookies = self.driver.get_cookies()
        with open('cookies.yaml', 'w', encoding='utf-8') as f:
            yaml.safe_dump(data=cookies, stream=f)

    def test_login(self):
        self.driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx")
        # 读取文件中的cookie
        with open('cookies.yaml', 'r') as f:
            cookie = yaml.safe_load(f)
        # 植入cookie信息
        for i in cookie:
            self.driver.add_cookie(i)
        self.driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx")
        print(cookie)

    def test_add_member(self):
        faker1 = Faker("zh-Cn")
        username = faker1.name()
        id = faker1.ssn()
        num = faker1.phone_number()

        self.driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx")
        with open('cookies.yaml', 'r') as f:
            cookie = yaml.safe_load(f)
        for i in cookie:
            self.driver.add_cookie(i)
        self.driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx")

        WebDriverWait(self.driver, 5).until(
            expected_conditions.element_to_be_clickable((By.CLASS_NAME, "index_service_cnt_item_title"))
        ).click()
        # self.driver.find_element(By.CLASS_NAME,"index_service_cnt_item_title").click()
        self.driver.find_element(By.ID, "username").send_keys(username)
        self.driver.find_element(By.ID, "memberAdd_acctid").send_keys(id)
        self.driver.find_element(By.ID, "memberAdd_phone").send_keys(num)
        self.driver.find_elements(By.CSS_SELECTOR, ".js_btn_save")[0].click()
        res = WebDriverWait(self.driver, 10).until(
            expected_conditions.visibility_of_element_located((By.ID, "js_tips"))
        ).text
        res = self.driver.find_element(By.ID, "js_tips").text
        assert "保存成功" == res
