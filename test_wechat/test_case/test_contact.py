# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_contact.py
# date:2022/7/31
from faker import Faker
from test_wechat.page.login_page import LoginPage


class TestContact:
    def setup_class(self):
        faker1 = Faker("zh_Cn")
        self.username = faker1.name()
        self.acctid = faker1.ssn()
        self.mobile = faker1.phone_number()
        # 打开登录页
        self.login_page = LoginPage()

    def teardown_class(self):
        # self.login_page.do_quit()
        pass

    def test_add_member(self):
        # 添加成员
        member_page = self.login_page.login()\
            .click_add_member()\
            .write_member_info(self.username,self.acctid,self.mobile)
        value = member_page.get_tips()
        # 调用获取成员列表的方法，返回姓名列表
        list_members = member_page.get_members()
        assert self.username in list_members
        assert "保存成功" == value


    def test_add_contact(self):
        # 添加部门
        contact_page = self.login_page.login()\
            .click_add_member()\
            .write_party_info(self.username)
        value = contact_page.get_tips()
        list_contact = contact_page.get_partys()
        assert self.username in list_contact
        assert "新建部门成功" == value


