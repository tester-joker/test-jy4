# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:contact_page.py
# date:2022/7/31
import time

from selenium.webdriver.common.by import By
from test_wechat.page.base_page import BasePage


class ContactPage(BasePage):
    """通讯录界面"""
    # 添加成员属性
    __INPUT_USERNAME = (By.ID,"username")
    __INPUT_ACCTID = (By.ID,"memberAdd_acctid")
    __INPUT_PHONE = (By.ID, "memberAdd_phone")
    __BTN_SAVE = (By.CSS_SELECTOR, ".js_btn_save")
    # 获取成员列表
    __TEXT_MEMBER_INFO = (By.CSS_SELECTOR,"#member_list>tr>td:nth-child(2)")

    # 添加部门属性
    __BTN_ADD = (By.XPATH,"//*[@class='member_colLeft_top_addBtnWrap js_create_dropdown']")
    __BTN_ADD_PARTY = (By.CSS_SELECTOR,".js_create_party")
    __INPUT_PARTY_NAME = (By.CSS_SELECTOR,'[name=name]')
    __BTN_PARENT_PARTY_NAME = (By.CSS_SELECTOR,'.js_parent_party_name')
    __BTN_PARTY_NAME =(By.XPATH,"//div[@class='inputDlg_item']//a[text()='唐山']")
    __BTN_SUBMIT = (By.XPATH,"//a[text()='确定']")
    # 获取部门列表
    __TEXT_CONTACT_INFO = (By.CSS_SELECTOR, "ul[role='group'] ul[role='group']>li>a")

    # 弹条属性
    __TEXT_TIPS = (By.ID,"js_tips")

    def write_member_info(self,username,acctid,mobile):
        """填写成员信息"""
        # 输入用户名
        self.do_send_keys(username,self.__INPUT_USERNAME)
        # 输入acctid
        self.do_send_keys(acctid,self.__INPUT_ACCTID)
        # 输入手机号
        self.do_send_keys(mobile,self.__INPUT_PHONE)
        # 点击保存
        self.do_finds(self.__BTN_SAVE)[0].click()
        return self

    def write_party_info(self,name):
        """填写部门信息"""
        # 点击加号
        self.do_find(self.__BTN_ADD).click()
        # 点击添加部门
        self.do_find(self.__BTN_ADD_PARTY).click()
        # 填写部门名称
        self.do_send_keys(name,self.__INPUT_PARTY_NAME)
        # 点击所属部门
        self.do_find(self.__BTN_PARENT_PARTY_NAME).click()
        # 选择部门
        self.do_find(self.__BTN_PARTY_NAME).click()
        # 点击确定
        self.do_find(self.__BTN_SUBMIT).click()
        return self


    def get_tips(self):
        """获取tips文本"""
        self.wait_visible(self.__TEXT_TIPS)
        value = self.do_find(self.__TEXT_TIPS).text
        return value

    def get_members(self):
        """获取成员列表，返回元素对象"""
        list_member_info =self.do_finds(self.__TEXT_MEMBER_INFO)
        # 设置存储姓名的列表
        list_members = []
        for i in list_member_info:
            list_members.append(i.text)
        return list_members

    def get_partys(self):
        """获取部门列表的所有名称"""
        list_contact_info = self.do_finds(self.__TEXT_CONTACT_INFO)
        list_contact = []
        for i in list_contact_info:
            list_contact.append(i.text)
        return list_contact