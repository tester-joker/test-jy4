# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:base_page.py
# date:2022/7/31
from selenium import webdriver
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class BasePage:
    # 主要用来与浏览器的driver交互
    def __init__(self,base_driver=None):
        if base_driver:
            self.driver = base_driver
        else:
            self.driver = webdriver.Chrome()
            self.driver.implicitly_wait(3)
            self.driver.maximize_window()

    def do_find(self,by):
        return self.driver.find_element(*by)

    def do_finds(self,by):
        # 获取多个元素
        return self.driver.find_elements(*by)

    def do_send_keys(self,value,by):
        # 输入文本
        ele = self.do_find(by)
        ele.clear()
        ele.send_keys(value)

    def do_quit(self):
        self.driver.quit()

    def wait_visible(self,locator):
        # 封装显示等待
        return WebDriverWait(self.driver,10).until(
            expected_conditions.visibility_of_element_located(locator)
        )