# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:home_page.py
# date:2022/7/31
from selenium.webdriver.common.by import By
from test_wechat.page.base_page import BasePage


class HomePage(BasePage):
    """首页"""
    __BTN_ADD = (By.LINK_TEXT,"添加成员")
    # 定位通讯录
    __BTN_CONTACT = (By.ID,"menu_contacts")

    def click_add_member(self):
        # 点击添加成员
        self.do_find(self.__BTN_ADD).click()
        from test_wechat.page.contact_page import ContactPage
        return ContactPage(self.driver)

    def click_contact(self):
        # 点击添加通讯录
        self.do_find(self.__BTN_CONTACT).click()
        from test_wechat.page.contact_page import ContactPage
        return ContactPage(self.driver)