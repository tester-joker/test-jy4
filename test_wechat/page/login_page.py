# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:login_page.py
# date:2022/7/31
import yaml

from test_wechat.page.base_page import BasePage
from test_wechat.page.home_page import HomePage


class LoginPage(BasePage):
    # 登录页
    def login(self):
        # 访问企业微信首页
        self.driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx")
        # 获取本地cookie
        with open("../cookies.yaml","r") as f:
            cookies = yaml.safe_load(f)
        for ck in cookies:
            self.driver.add_cookie(ck)
        # 再次访问首页
        self.driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx")
        return HomePage(self.driver)