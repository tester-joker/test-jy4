# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_encryption.py
# date:2022/9/20
import json
import requests
import base64


class ApiRequest:
    def send(self,data:dict):
        res = requests.request(data['method'],data['url'],headers=data['headers'])
        if data['encoding'] == 'base64':
            return json.loads(base64.b64decode(res.content))
        # 这里的private是自定义的算法
        elif data['encoding'] == 'private':
            # 把加密过后的响应值发给第三方服务，让第三方去做解密
            return requests.post('url',data=res.content)