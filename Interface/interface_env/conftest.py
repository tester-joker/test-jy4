# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:conftest.py
# date:2022/9/20
from _pytest.config import Config
from _pytest.config.argparsing import Parser

global_env = {}


# hook函数：添加命令行参数
def pytest_addoption(parser: Parser):
    # 注册一个命令行组
    hogwarts = parser.getgroup("hogwarts")
    # 第一个参数为指定的命令行的参数的形式
    # pytest .\temo.py --browser=chrome
    # 注册一个命令行参数
    hogwarts.addoption("--env",
                       # 参数默认值
                       default="test",
                       # 存储的变量
                       dest="env",
                       # 参数的描述信息
                       help="设置接口自动化测试默认的环境")


# 获取设置的命令行参数
def pytest_configure(config: Config):
    default_env = config.getoption("--env")
    # 写入一个env 的字典
    tmp = {'env':default_env}
    global_env.update(tmp)