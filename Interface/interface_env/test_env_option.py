# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_env_option.py
# date:2022/9/20
import requests
import yaml
# 设置临时环境变量
# win：set interface_env=test
# mac：export interface_env=test
from Interface.interface_env.conftest import global_env


class TestEnvOption:
    def setup_class(self):
        # 获取命令行参数env的值
        path_env = global_env.get('env')
        env = yaml.safe_load(open(f'{path_env}.yaml',encoding='utf-8'))
        self.base_url = env['base_url']
        # 如果使用powershell，需要使用 $env:interface_env="test" 命令设置环境变量

    def test_dev(self):
        """
        验证是否为开发环境
        return:
        """
        r = requests.get(self.base_url+'get')
        print(r.json())
        # 假设 httpbin.ceshiren.com 是开发环境 ，那么断言就会成功
        assert r.json()['headers']['Host'] == 'httpbin.ceshiren.com'

    def test_env(self):
        """
        验证是否为测试环境
        return:
        """
        r = requests.get(self.base_url+'get')
        print(r.json())
        # 假设 https://httpbin.org/ 是测试环境 ，那么断言就会成功
        assert r.json()['headers']['Host'] == 'httpbin.org'
