# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_muliti_env.py
# date:2022/9/20
import os
import requests
import yaml

# 设置临时环境变量
# win：set interface_env=test
# mac：export interface_env=test


class TestMulitiEnv:
    def setup_class(self):
        # 只指定path，不指定url
        # 从yaml文件读取数据
        # 优化：从环境变量读取名为interface_env的配置环境，通过default设置默认值
        path_env = os.getenv("interface_env", default='test')
        # 读取对应文件内容
        env = yaml.safe_load(open(f'{path_env}.yaml',encoding='utf-8'))
        self.base_url = env['base_url']
        # 如果使用powershell，需要使用 $env:interface_env="test" 命令设置环境变量

    def test_dev(self):
        """
        验证是否为开发环境
        return:
        """
        r = requests.get(self.base_url+'get')
        print(r.json())
        # 假设 httpbin.ceshiren.com 是开发环境 ，那么断言就会成功
        assert r.json()['headers']['Host'] == 'httpbin.ceshiren.com'

    def test_env(self):
        """
        验证是否为测试环境
        return:
        """
        r = requests.get(self.base_url+'get')
        print(r.json())
        # 假设 https://httpbin.org/ 是测试环境 ，那么断言就会成功
        assert r.json()['headers']['Host'] == 'httpbin.org'
