# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_wechat.py
# date:2022/9/25
import pytest
import requests


class TestDepartment:
    def setup_class(self):
        # 获取access_token值
        corpid = "wwa238703bb5cc3035"
        secert = "70ZcTeq40M0UAD6Rj_3I5KBsYXMsuKLfWOx0ccDxizQ"
        url = f"https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={corpid}&corpsecret={secert}"
        # 发出请求
        res = requests.get(url=url)
        self.token = res.json().get('access_token')

    @pytest.mark.parametrize('name,name_en,parent_id,order,department_id,result',[
        ("深圳研发中心","RDGZ",1,1,10,0),
        ("深圳研发中心gdfhadfgadfhadfhadfhadfhfd", "RDGZ1", 1, 1, 11, 0),
        ("深圳研发中心dafhadfhadfhadfgasdfasdffds", "RDGZ2", 1, 1, 12, 60001)
    ])
    def test_add_department(self,name,name_en,parent_id,order,department_id,result):
        department_data = {
                "name": name,
                "name_en": name_en,
                "parentid": parent_id,
                "order": order,
                "id": department_id
        }
        url = f"https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token={self.token}"
        res = requests.post(url=url,json=department_data)
        assert res.json().get('errcode') == result