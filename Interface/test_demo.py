# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_demo.py
# date:2022/9/9
import requests
from hamcrest import *
from jsonpath import jsonpath

class TestDemo:
    def test_query(self):
        pay = {
            'a':"hogwarts",
            'test':123
        }
        re = requests.get('https://httpbin.testing-studio.com/get',params=pay)
        print(re.text)
        assert re.status_code == 200

    # 提交form表单，一般是登录时用
    def test_post_form(self):
        pay = {
            'a':"hogwarts",
            'test':123
        }
        re = requests.post('https://httpbin.testing-studio.com/post',data=pay)
        print(re.text)
        assert re.status_code == 200

    def test_header(self):
        pay = {
            'a':"demo"
        }
        re = requests.get('https://httpbin.testing-studio.com/get',headers=pay)
        print(re.text)
        assert re.status_code == 200

    def test_json(self):
        pay = {
            'a':"demo"
        }
        re = requests.post('https://httpbin.testing-studio.com/post',json=pay)
        print(re.text)
        assert re.status_code == 200
        assert re.json()['json']['a'] == "demo"

    def test_hamcrest(self):
        # hamcrest 断言体系（用于解决复杂场景的断言）
        pay = {
            'a':"demo"
        }
        re = requests.post('https://httpbin.testing-studio.com/post',json=pay)
        print(re.text)
        assert_that(re.json()['json']['a'],equal_to("demo"))

    def test_hogwarts_json(self):
        re = requests.get('https://ceshiren.com/categories.json')
        print(re.text)
        assert re.status_code == 200
        assert re.json()['category_list']['categories'][0]['name'] == '提问区'
        assert jsonpath(re.json(),'$..name')[0] == '提问区'

    def test_proxy(self):
        # 设置代理
        proxy = {
            'http': 'http://127.0.0.1:8888',
            'https': 'http://127.0.0.1:8888'
        }
        # verify证书认证，改为false即为不认证
        requests.post(url='https://httpbin.ceshiren.com/post', proxies=proxy, verify=False)

    def test_cookie(self):
        url = 'https://httpbin.ceshiren.com/cookies'
        header = {
            'User-Agent': "demo"
        }
        cookie_demo = {
            'hogwarts':'school',
            'demo':'123'
        }
        re = requests.get(url=url, headers=header,cookies=cookie_demo)
        # 打印请求头信息
        print(re.request.headers)
        # 打印响应头信息
        print(re.headers)

    def test_form(self):
        proxy = {
            'http':'http://127.0.0.1:8888',
            'https':'http://127.0.0.1:8888'
        }
        url = 'https://httpbin.ceshiren.com/post'
        data = {
            'hogwarts':'school',
            'demo':'123'
        }
        # form请求
        re = requests.post(url=url, data=data,proxies=proxy,verify=False)
        print(re.json())
        # json请求
        r = requests.post(url=url, json=data,proxies=proxy,verify=False)
        print(r.json())
        # 请求的Content-Type不同，响应内容的字段不同

    def test_file(self):
        proxy = {
            'http': 'http://127.0.0.1:8888',
            'https': 'http://127.0.0.1:8888'
        }
        url = 'https://httpbin.ceshiren.com/post'
        file = {'hogwarts_name':
                    open(r'C:\Users\Administrator\Desktop\file1.txt','rb')}
        # 通过value元组传递，实现指定filename的需求
        file2 = {'hogwarts_name':
                     ('hogwarts',open(r'C:\Users\Administrator\Desktop\file1.txt', 'rb'))}
        re = requests.post(url=url,
                           files=file2, proxies=proxy, verify=False)
        print(re.json())