# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:conftest.py
# date:2022/9/19
import pytest


# 利用fixtrue实现参数化
@pytest.fixture(params=[{"name": 'tester', "id": 1}, {"name": 'joker', "id": 2}], scope='class')
def get_paramesize(request):
    # 注意，这里是param，不是params
    name = request.param['name']
    tag_id = request.param['id']
    print(f"标签名称为{name},标签id为{tag_id}")
    return name, tag_id
