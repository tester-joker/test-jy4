# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_wechart_tag.py
# date:2022/9/19
import pytest
import requests


class TestDemo:
    def setup_class(self):
        # 获取access_token值
        corpid = "wwa238703bb5cc3035"
        secert = "70ZcTeq40M0UAD6Rj_3I5KBsYXMsuKLfWOx0ccDxizQ"
        url = f"https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={corpid}&corpsecret={secert}"
        # 发出请求
        res = requests.get(url=url)
        self.token = res.json().get('access_token')

    # 添加标签
    def test_add_tag(self, get_paramesize):
        tag_data = {
            "tagname": get_paramesize[0],
            "tagid": get_paramesize[1]
        }
        url = f"https://qyapi.weixin.qq.com/cgi-bin/tag/create?access_token={self.token}"
        res = requests.post(url=url, json=tag_data)
        print(res.json())
        assert res.json().get('errcode') == 0

    # 修改标签名称
    def test_update_tag(self, get_paramesize):
        tag_data = {
            "tagid": get_paramesize[1],
            "tagname": get_paramesize[0] + f"{get_paramesize[1]}"
        }

        url = "https://qyapi.weixin.qq.com/cgi-bin/tag/update"
        update_data = {"access_token": self.token}
        res = requests.post(url=url, json=tag_data, params=update_data)
        print(res.json())
        assert res.json().get('errcode') == 0
        assert res.json().get('errmsg') == 'updated'

    # 获取标签列表
    def test_tag_list(self):
        # 获取部门列表
        list_url = f"https://qyapi.weixin.qq.com/cgi-bin/tag/list?access_token={self.token}"
        res = requests.get(url=list_url)
        print(res.json())
        assert res.json().get('errcode') == 0
        assert res.json().get('errmsg') == 'ok'

    # 删除标签
    def test_delete_tag(self, get_paramesize):
        list_url = "https://qyapi.weixin.qq.com/cgi-bin/tag/delete"
        update_data = {"access_token": self.token,
                       "tagid": get_paramesize[1]
                       }
        res = requests.get(url=list_url, params=update_data)
        print(res.json())
        assert res.json().get('errcode') == 0
        assert res.json().get('errmsg') == 'deleted'
