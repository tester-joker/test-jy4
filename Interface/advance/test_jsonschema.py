# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_jsonschema.py
# date:2022/9/26
import json

from jsonschema import validate


def schema_validate(obj, schema):
    try:
        validate(instance=obj, schema=schema)
        return True
    except Exception as e:
        # raise e
        print(e)
        return False


def test_validate():
    _schema = json.load(open("demo_schema.json", encoding="utf-8"))
    assert schema_validate({"a": "1", "b": "12", "c": 1}, _schema)
