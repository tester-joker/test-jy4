# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:database_demo.py
# date:2022/9/26
import pymysql as pymysql


def query_db():
    conn = pymysql.Connect(host="47.92.149.0", port=3366, database="test_db",
                           user="root", password="123456",
                           charset="utf8")
    cursor = conn.cursor()
    sql = "select * from user_info"
    cursor.execute(sql)
    print("行数:", cursor.rowcount)
    # 获取行数
    datas = cursor.fetchall()
    print("查询到的数据为:", datas)  # 获取多条数据
    cursor.close()
    conn.close()
    return datas


if __name__ == '__main__':
    query_db()
