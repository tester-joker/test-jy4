# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_protocol_res.py
# date:2022/9/21
import requests
import xmltodict


def test_xml_to_dict():
    res = requests.get("https://www.nasa.gov/rss/dyn/lg_image_of_the_day.rss")
    # xml格式转成dict格式
    res_dict = xmltodict.parse(res.text)
    print(res_dict)