# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_response_todict.py
# date:2022/9/21
import requests
import xmltodict
from requests import Response


def test_response_to_dict():
    res = requests.get("https://www.nasa.gov/rss/dyn/lg_image_of_the_day.rss")
    # xml格式转成dict格式
    res_dict = response_to_dice(res)
    # 判断响应格式是否为dict
    assert isinstance(res_dict,dict)


def response_to_dice(response:Response):
    res_text = response.text
    # 判断响应文本信息是否以<?xml 开头
    if res_text.startswith("<?xml"):
        final_dict = xmltodict.parse(res_text)
    elif res_text.startswith("<!DOCTYPE"):
        final_dict = 'html'
    # 如果不是其他格式，那肯定就是json格式
    else:
        final_dict = response.json()
    return final_dict