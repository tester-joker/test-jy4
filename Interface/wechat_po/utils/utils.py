# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:utils.py
# date:2022/9/25
from jsonpath import jsonpath


class Utils:

    @classmethod
    def base_jsonpath(cls, obj, json_expr):
        '''
        封装jsonpath断言
        :@obj: 响应的内容
        :@json_expr: jsonpath表达式
        :return: jsonpath匹配的结果
        '''
        return jsonpath(obj, json_expr)
