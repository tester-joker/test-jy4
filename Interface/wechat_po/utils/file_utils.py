# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:file_utils.py
# date:2022/9/25
import os.path

import yaml

class FileUtils:
    @classmethod
    def get_yaml_data(cls,file_path):
        """封装yaml读取的方法"""
        with open(file_path,encoding='utf-8') as f:
            data = yaml.safe_load(f)
        return data

    @classmethod
    def get_path(cls):
        # 获取当前文件所在路径
        # path = os.path.abspath(__file__)
        # 获取当前文件的上级路径
        path = os.path.dirname(os.path.abspath(__file__))
        return os.path.dirname(path)

