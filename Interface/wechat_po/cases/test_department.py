# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_department.py
# date:2022/9/25
import allure

from Interface.wechat_po.apis.department import Department
from Interface.wechat_po.utils.utils import Utils


class TestDepartment:
    def setup_class(self):
        # 实例化部门类
        self.department = Department()
        self.department_id = 12
        self.creat_data = {
                "name": "广州研发中心",
                "name_en": "RDGZ",
                "parentid": 1,
                "order": 1,
                "id": self.department_id
        }
        self.update_data = {
           "id": self.department_id,
           "name": "深圳研发中心",
           "name_en": "RDGZ_update",
           "parentid": 1,
           "order": 1
        }

    @allure.story("部门管理场景用例")
    def test_department(self):
        '''创建部门 -> 更新部门 -> 删除部门'''
        with allure.step('创建部门'):
            self.department.create(self.creat_data)
        with allure.step("查询创建部门的结果"):
            add_list = self.department.get()
            # 断言
            # department_list = [data.get('id') for data in add_list]
            assert self.department_id in Utils.base_jsonpath(add_list, "$..id")

        with allure.step("更新部门"):
            self.department.update(self.update_data)
        with allure.step("查询更新部门的结果"):
            update_list = self.department.get().get('department_id')
            # 断言
            assert self.department_id in Utils.base_jsonpath(update_list, "$..id")

        with allure.step("删除部门"):
            self.department.delete(self.department_id)
        with allure.step("查询创建部门的结果"):
            del_list = self.department.get().get('department_id')
            assert self.department_id not in Utils.base_jsonpath(del_list, "$..id")



