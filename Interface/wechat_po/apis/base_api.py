# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:base_api.py
# date:2022/9/25
import os
import requests

from Interface.wechat_po.utils.log_utils import logger
from Interface.wechat_po.utils.file_utils import FileUtils


class BaseApi:
    conf_data = FileUtils.get_yaml_data(os.sep.join([FileUtils.get_path(), 'datas/config.yaml']))
    test_env = os.environ.get('test_env')
    BaseUrl = conf_data.get('wework_env').get(test_env)

    def send(self, method, url, **kwargs):
        """
        对requests进行二次封装
        如果涉及到技术的更换，例如不需要用requests库了，那只需要更改这里的代码即可
        return：接口的响应结果
        """
        url = self.BaseUrl+url
        logger.info(f"发起接口调用使用的 method 为：{method}")
        logger.info(f"发起接口调用使用的 url 为：{url}")
        logger.info(f"发起接口调用使用的 params 为：{kwargs.get('params')}")
        logger.info(f"发起接口调用使用的 data 为：{kwargs.get('data')}")
        logger.info(f"发起接口调用使用的 json 为：{kwargs.get('json')}")
        res = requests.request(method, url, **kwargs)
        logger.info(f"接口返回的 json 为：{res.json()}")
        return res
