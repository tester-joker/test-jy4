# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:department.py
# date:2022/9/25
'''
描述部门的管理，只关注业务
包括增删改查等功能
'''

from Interface.wechat_po.apis.contract import Contract


class Department(Contract):
    '''需要继承wework，需要获取token'''
    def create(self,data):
        create_url = "/department/create"
        param = {"access_token": self.token}
        res = self.send(method='post',url=create_url,params=param,json=data)
        return res.json()

    def update(self,data):
        update_url = "/department/update"
        update_data = {"access_token": self.token}
        res = self.send(method='post',url=update_url, json=data, params=update_data)
        return res.json()

    def get(self):
        list_url = "/department/simplelist"
        param = {"access_token": self.token}
        res = self.send(method='get',url=list_url,params=param)
        return res.json()

    def delete(self, del_id):
        del_url = "/department/delete"
        update_data = {"access_token": self.token,
                       "id": del_id}
        res = self.send(method='get',url=del_url, params=update_data)
        return res.json()