# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:tag.py
# date:2022/9/25

'''
描述标签的管理，只关注业务
包括增删改查等功能
'''
from Interface.wechat_po.apis.wework import Wework


class Tag(Wework):
    '''需要继承wework，需要获取token'''
    def create(self):
        pass

    def update(self):
        pass

    def get(self):
        pass

    def delete(self):
        pass