# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:contract.py
# date:2022/9/25
import os

from Interface.wechat_po.apis.wework import Wework
from Interface.wechat_po.utils.file_utils import FileUtils

'''
抽离出来各个模块的token数据准备
比如通讯录管理和应用管理的corpid和secret值不同，所以可以将各个模块管理抽离出来，方便维护
'''


class Contract(Wework):
    # 获取通讯录管理的token
    def __init__(self):
        # 路径拼接
        # conf_data = FileUtils.get_yaml_data(os.sep.join([FileUtils.get_path(), 'datas/config.yaml']))
        corpid = self.conf_data.get('mail').get('corpid')
        secret = self.conf_data.get('mail').get('secret')
        self.token = self.get_access_token(corpid, secret)
