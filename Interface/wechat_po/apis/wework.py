# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:wework.py
# date:2022/9/25
from Interface.wechat_po.apis.base_api import BaseApi


class Wework(BaseApi):
    def get_access_token(self, corpid, secert):
        '''
        获取access_token的值
        :return: token
        '''
        token_url = "/gettoken"
        param = {
            "corpid" : corpid,
            "corpsecret" : secert
        }
        # 发出请求
        res = self.send(method='get', url=token_url,params=param)
        token = res.json().get('access_token')
        return token
