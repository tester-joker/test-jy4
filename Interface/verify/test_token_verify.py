# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_token_verify.py
# date:2022/9/21
import requests


class TestTokenVerify:
    def setup_class(self):
        self.proxy = {'http':'http://127.0.0.1:8888',
                    'https':'http://127.0.0.1:8888'
                    }

    # token鉴权
    def test_litemall_token(self):
        r = requests.post("https://litemall.hogwarts.ceshiren.com/admin/auth/login",
                          json={"username":'admin123',
                                "password":"admin123",
                                "code":""},proxies=self.proxy,verify=False)
        token = r.json()['data']['token']
        print(token)

        r2 = requests.get("https://litemall.hogwarts.ceshiren.com/admin/profile/nnotice",
                          headers={"X-Litemall-Admin-Token":token},proxies=self.proxy,verify=False)
        print(r2.json())
