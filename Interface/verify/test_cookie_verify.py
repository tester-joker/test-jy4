# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_cookie_verify.py
# date:2022/9/21
import requests


class TestCookieVerify:
    def setup_class(self):
        self.proxy={'http':'http://127.0.0.1:8888',
                    'https':'http://127.0.0.1:8888'
                    }

    def test_cookie(self):
        r= requests.get("https://httpbin.ceshiren.com/cookies",cookies={'hogwarts':'ceshiren'})
        print(r.json())

    def test_cookies_without_session(self):
        '''
        1. 获取cookie
        2. set cookie 设置cookie
        3. 再次获取cookie，查看是否设置成功
        :return:
        '''
        r = requests.get("https://httpbin.ceshiren.com/cookies",proxies=self.proxy,verify=False)
        print(f"第一次响应值为{r.json()}")

        r2 = requests.get("https://httpbin.ceshiren.com/cookies/set/username/325",proxies=self.proxy,verify=False)
        print(f"第一次响应值为{r2.json()}")
        r3 = requests.get("https://httpbin.ceshiren.com/cookies",proxies=self.proxy,verify=False)
        print(f"第一次响应值为{r3.json()}")

    def test_session(self):
        # cookie的鉴权可以用session处理，这样的话只需要登录一次，下一次访问也会携带登录的cookie信息
        # requests.session可以把cookie信息带到后面的每一个请求中
        req = requests.session()
        r = req.get("https://httpbin.ceshiren.com/cookies", proxies=self.proxy, verify=False)
        print(f"第一次响应值为{r.json()}")
        r2 = req.get("https://httpbin.ceshiren.com/cookies/set/username/325", proxies=self.proxy, verify=False)
        print(f"第一次响应值为{r2.json()}")
        r3 = req.get("https://httpbin.ceshiren.com/cookies", proxies=self.proxy, verify=False)
        print(f"第一次响应值为{r3.json()}")