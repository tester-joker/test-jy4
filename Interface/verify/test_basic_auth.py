# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_basic_auth.py
# date:2022/9/22
import requests
from requests.auth import HTTPBasicAuth


class TestBasicAuth:
    def setup_class(self):
        self.proxy={'http':'http://127.0.0.1:8888',
                    'https':'http://127.0.0.1:8888'
                    }

    # auth鉴权
    def test_basic_auth(self):
        res = requests.get("https://httpbin.ceshiren.com/basic-auth/joker/123",
                           auth=HTTPBasicAuth('joker','123'),
                           proxies=self.proxy,verify=False)
        print(res.json())