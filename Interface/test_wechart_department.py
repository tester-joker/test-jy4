# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_wechart_department.py
# date:2022/9/18

import requests

# 企业微信
class TestDemo:
    def setup_class(self):
        # 获取access_token值
        corpid = "wwa238703bb5cc3035"
        secert = "70ZcTeq40M0UAD6Rj_3I5KBsYXMsuKLfWOx0ccDxizQ"
        url = f"https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={corpid}&corpsecret={secert}"
        # 发出请求
        res = requests.get(url=url)
        self.token = res.json().get('access_token')


    # post请求
    def test_add_department(self):
        department_data = {
                "name": "广州研发中心",
                "name_en": "RDGZ",
                "parentid": 1,
                "order": 1,
                "id": 10
        }
        url = f"https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token={self.token}"
        res = requests.post(url=url,json=department_data)
        print(res.json())
        assert res.json().get('errcode') == 0

    def test_update_department(self):
        department_data = {
           "id": 10,
           "name": "深圳研发中心",
           "name_en": "RDGZ_update",
           "parentid": 1,
           "order": 1
        }
        url = "https://qyapi.weixin.qq.com/cgi-bin/department/update"
        update_data ={"access_token": self.token}
        res = requests.post(url=url, json=department_data,params=update_data)
        print(res.json())
        assert res.json().get('errcode') == 0

    def test_department_list(self):
        # 获取部门列表
        list_url = f"https://qyapi.weixin.qq.com/cgi-bin/department/simplelist?access_token={self.token}"
        res = requests.get(url=list_url)
        print(res.json())
        assert res.json().get('errcode') == 0
        assert res.json().get('errmsg') == 'ok'

    def test_delete_department(self):
        list_url = f"https://qyapi.weixin.qq.com/cgi-bin/department/delete"
        update_data = {"access_token": self.token,
                       "id":10
                       }
        res = requests.get(url=list_url,params=update_data)
        print(res.json())
        assert res.json().get('errcode') == 0
        assert res.json().get('errmsg') == 'deleted'
