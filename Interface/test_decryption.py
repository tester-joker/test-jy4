# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_decryption.py
# date:2022/9/20

import base64
import json
import requests

from Interface import test_encryption


req_data = {
        'method':'get',
        'url':"http://127.0.0.1:9999/demo.txt",
        'headers':None,
        # 控制加解密算法
        'encoding':'base64'
    }


def test_send():
    api = test_encryption.ApiRequest()
    print(api.send(req_data))


def test_encode(self):
    url = "http://127.0.0.1:9999/demo.txt"
    r = requests.get(url=url)
    # r.content获取二进制响应结果，base64解密，通过json.loads转成json格式
    res = json.loads(base64.b64decode(r.content))
    print(res)