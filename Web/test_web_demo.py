# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_web_demo.py
# date:2022/7/21

# 显示等待
# import time
# from selenium import webdriver
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.wait import WebDriverWait
#
# driver = webdriver.Chrome()
# driver.get("https://vip.ceshiren.com/#/ui_study")
# # 点击的目标按钮
# # 期望出现的元素
# def muliti_click(target_element,next_element):
#     def _predicate(driver):
#         driver.find_element(*target_element).click()
#         return driver.find_element(*next_element)
#         # 结果为找到,return一个webelement对象
#         # 如果没有找到，会被until里的try捕获异常，报错，但是会一直循环，直到找到或者超时
#     return _predicate
# # 封装需求，一直点击按钮，直到下一个页面出现为止
#
# WebDriverWait(driver,5).until(
#     muliti_click(
#         # 这里需要两个括号，因为调用的函数内有解包操作
#         (By.ID,"primary_btn"),(By.XPATH,"//*[text()='该弹框点击两次后才会弹出']")
#     ))
# # driver.find_element(By.ID,"primary_btn").click()
# time.sleep(3)
# driver.close()


# 高级控件交互
# from time import sleep
# from selenium import webdriver
# from selenium.webdriver import ActionChains, Keys
# from selenium.webdriver.common.by import By
#
# class TestActionChains():
#     def setup(self):
#         self.driver = webdriver.Chrome()
#         self.driver.implicitly_wait(5)
#         self.driver.maximize_window()
#     def teardown(self):
#         self.driver.quit()
#
#     def test_case_click(self):
#         self.driver.get("http://sahitest.com/demo/clicks.htm")
#         element_click = self.driver.find_element(By.XPATH,"//input[@value='click me']")
#         element_doubleclick = self.driver.find_element(By.XPATH,"//input[@value='dbl click me']")
#         element_rightclick = self.driver.find_element(By.XPATH, "//input[@value='right click me']")
#         action = ActionChains(self.driver)
#         action.click(element_click)
#         action.context_click(element_rightclick)
#         action.double_click(element_doubleclick)
#         sleep(3)
#         action.perform()
#         sleep(3)
#     def test_move_to_element(self):
#         self.driver.get("http://www.baidu.com")
#         ele = self.driver.find_element(By.ID,"s-usersetting-top")
#         action = ActionChains(self.driver)
#         action.move_to_element(ele)
#         action.perform()
#         sleep(3)
#     def test_dragdrop(self):
#         self.driver.get("http://sahitest.com/demo/dragDropMooTools.htm")
#         drag_element = self.driver.find_element(By.ID,"dragger")
#         drop_element = self.driver.find_element(By.XPATH,"/html/body/div[2]")
#         action = ActionChains(self.driver)
#         action.drag_and_drop(drag_element,drop_element).perform()
#         action.click_and_hold(drag_element).move_to_element(drop_element).release().perform()
#         sleep(3)
#     def test_keys(self):
#         self.driver.get("http://sahitest.com/demo/label.htm")
#         ele = self.driver.find_element(By.XPATH,"/html/body/label[1]/input")
#         # 点击输入框
#         ele.click()
#         action = ActionChains(self.driver)
#         action.send_keys("username").pause(1)
#         # 输入空格
#         action.send_keys(Keys.SPACE).pause(1)
#         action.send_keys("tom").pause(1)
#         # 删除一位
#         action.send_keys(Keys.BACK_SPACE).perform()
#         sleep(3)
import time

from selenium import webdriver

class Test_TouchAction():
    def setup(self):
        opt = webdriver.ChromeOptions()
        # add_experimental_option添加了一个实验选项，该选项传递给 chrome
        opt.add_experimental_option('w3c', False)
        # ChromeOptions是chromedriver支持的浏览器启动选项。
        self.driver = webdriver.Chrome(chrome_options=opt)

    def teardown(self):
        self.driver.quit()

    def test_case(self):
        self.driver.get("http://www.baidu.com")
        self.driver.maximize_window()
        self.driver.implicitly_wait(10)
        self.driver.find_element_by_id("kw").send_keys("selenium测试")
        action_search = TouchActions(self.driver)
        # tap 在指定元素上敲击
        action_search.tap(self.driver.find_element_by_id("su")).perform()
        el = self.driver.find_element_by_id("su")
        action = TouchActions(self.driver)
        # 从某个元素位置开始手势点击并滚动（向下滑动为负数，向上滑动为正数）,向上滑动10000个像素
        # 触摸并从on_element元素开始滚动，按xoffset和yoffset移动。
        action.scroll_from_element(el,10,10000).perform()
        time.sleep(3)



from time import sleep
from selenium.webdriver import ActionChains, TouchActions
from selenium.webdriver.common.by import By
from Web.base import Base

class TestFrame(Base):
    def test_frame(self):
        self.driver.get("https://www.runoob.com/try/try.php?filename=jqueryui-api-droppable")
        self.driver.switch_to.frame("iframeResult")
        # 新版本的selenium已经删除了这种用法， 不推荐使用
        # self.driver.switch_to_frame("iframeResult")
        print(self.driver.find_element(By.ID,"draggable").text)
        self.driver.switch_to.default_content()
        print(self.driver.find_element(By.ID,"submitBTN").text)

from log.demo_logger import logger
class TestFile(Base):
    def test_file_upload(self):
        self.driver.get("https://image.baidu.com")
        self.driver.find_element(By.ID,"sttb").click()
        sleep(2)
        # 文件上传
        self.driver.find_element(By.ID,"stfile").send_keys(r"C:\Users\Administrator\Desktop\picture\1.jpg")


    def test_alert(self):
        self.driver.get("https://www.runoob.com/try/try.php?filename=jqueryui-api-droppable")
        self.driver.switch_to_frame("iframeResult")
        action = ActionChains(self.driver)
        drag = self.driver.find_element(By.ID, "draggable")
        drop = self.driver.find_element(By.ID,"droppable")
        # 点击拖拽到drop
        action.drag_and_drop(drag,drop).perform()
        sleep(2)
        print("点击 alert 确认")
        # 获取当前页面的警告框，接受警告框
        self.driver.switch_to_alert().accept()
        # 切换回默认的frame下，即切换回主文档
        self.driver.switch_to_default_content()
        self.driver.find_element(By.ID,"submitBTN").click()
        sleep(3)

class TestDataRecord(Base):
    def test_log_data_record(self):
        # 搜索内容
        search_content = "霍格沃兹测试开发"
        self.driver.get("https://www.sogou.com")
        self.driver.find_element(By.ID,"query").send_keys(search_content)
        logger.debug(f"搜索的信息为[{search_content}")
        self.driver.find_element(By.ID,"stb").click()
        search_res = self.driver.find_element(By.CSS_SELECTOR,"em")
        logger.info(f"预期结果为{search_content},实际结果为{search_res.text}")
        assert search_res.text == search_content

    def test_screen_shot_data_recode(self):
        search_content = "霍格沃兹测试开发"
        self.driver.get("https://www.sogou.com")
        self.driver.find_element(By.ID, "query").send_keys(search_content)
        logger.debug(f"搜索的信息为[{search_content}")
        self.driver.find_element(By.ID, "stb").click()
        search_res = self.driver.find_element(By.CSS_SELECTOR, "em")
        logger.info(f"预期结果为{search_content},实际结果为{search_res.text}")
        self.driver.save_screenshot("screen_shot_data_recode.png")
        assert search_res.text == search_content

    def test_page_source_data_recode(self):
        # 问题：产生了no such element 的错误
        # 解决方案：在报错的代码行之前打印page_source，确认定位的元素没有问题
        search_content = "霍格沃兹测试开发"
        self.driver.get("https://www.sogou.com")
        with open("recode.html","w",encoding="u8") as f:
            f.write(self.driver.page_source)
        self.driver.find_element(By.ID, "query").send_keys(search_content)
