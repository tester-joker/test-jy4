# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:web_demo.py
# date:2022/7/25
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

# 复用浏览器
# 定义配置的实例对象option
option = Options()
# 修改实例属性为debug模式启动的ip+窗口
option.debugger_address = "localhost:9222"
# 实例化driver的时候需要添加option配置
driver = webdriver.Chrome(options=option)
driver.get("https://www.baidu.com")
driver.find_element_by_id("kw").send_keys("hogwarts")

