# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_screenshot.py
# date:2022/7/28
import time
import allure
from selenium import webdriver
from selenium.webdriver.common.by import By
# 问题1：异常处理会影响用例本身的结果
# 解决方案：在exception之后把异常抛出
# 问题2：异常捕获处理代码和业务代码无关，不能耦合
# 解决方案：使用装饰器装饰用例，或者方法，就不会体现在源码中了。
# 问题3：需要driver实例截图/打印page_source，装饰器需要先去获取driver对象
# 解决方案：driver = args[0].driver
# 问题4：如果被装饰函数有返回值，就会丢失返回值
# 解决方案：return func(*args,**kwargs)
def ui_exception_record(func):
    def inner(*args,**kwargs):
        # 获取被装饰方法的self，也就是实例对象
        # 通过self可以拿到
        # 前提条件：被装饰的方法是一个实例方法，并且要有实例变量self.driver
        try:
            # 当被装饰方法/函数发生异常就捕获并做记录
            return func(*args,**kwargs)
        except Exception as e:
            # 获取driver要放在函数执行之后，只有出现报错了，才要去获取driver对象
            # 如果用了setup等方法，说明使用装饰器时，driver已经声明了，获取driver可以放在前面
            driver = args[0].driver
            print("出异常了")
            timestamp = int(time.time())
            image_path = f"./images/image_{timestamp}.PNG"
            page_source_path = f"./page_source/page_source_{timestamp}.html"
            # 截图
            driver.save_screenshot(image_path)
            # 记录page_source
            with open(page_source_path,"w",encoding="u8") as f:
                f.write(driver.page_source)
            # 将截图放到报告的数据中
            allure.attach.file(image_path,name="image",
                               attachment_type=allure.attachment_type.PNG)
            allure.attach.file(page_source_path,name="page_source",
                               attachment_type=allure.attachment_type.TEXT)
            raise e
    return inner

class TestWeb:
    def setup_class(self):
        self.driver = webdriver.Chrome()
    @ui_exception_record
    def test_baidu(self):
        self.driver.get("https://www.baidu.com")
        return self.driver.find_element(By.ID,"su1")
    def teardown_class(self):
        self.driver.quit()
