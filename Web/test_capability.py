# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_capability.py
# date:2022/7/31
import time
from selenium import webdriver
from selenium.webdriver.common.by import By

def test_ceshiren_capability():
    capability = {"platformName":"windows"}
    driver = webdriver.Chrome(desired_capabilities=capability)
    driver.get("https://ceshiren.com")

"""需要最新版的selenium才可以执行成功"""
def test_ceshiren2():
    hogwarts_grid_url = "https://selenium-node.hogwarts.ceshiren.com/wd/hub"
    capabilities = {"browserName":"chrome","browserVersion":"101.0"}
    # 配置信息
    # 实例化Remote，获取可以远程控制的driver实例对象
    # 通过 command_executor 配置selenium hub地址
    # 通过 desired_capabilities 添加配置信息
    driver = webdriver.Remote(
        command_executor=hogwarts_grid_url,
        desired_capabilities=capabilities)
    # driver = webdriver.Chrome()
    driver.implicitly_wait(5)
    driver.get("https://ceshiren.com/")
    text = driver.find_element(By.CSS_SELECTOR, ".login-button").text
    print(text)
    time.sleep(3)
    driver.quit()