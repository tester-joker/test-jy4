# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:test_browser.py
# date:2022/7/30

from selenium import webdriver


class TestBrowser:
    def setup_class(self):
        from Web.browser.conftest import web_env
        self.browser = web_env.get("browser")

    def test_ceshiren(self):
        print(f"获取到的浏览器信息为{self.browser}")
        if self.browser == 'firefox':
            self.driver = webdriver.Firefox()
        else:
            self.driver = webdriver.Chrome()
        self.driver.get("https://ceshiren.com")
        self.driver.quit()
