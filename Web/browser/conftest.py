# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:conftest.py
# date:2022/7/30
from _pytest.config import Config
from _pytest.config.argparsing import Parser

web_env={}


def pytest_addoption(parser:Parser):
    # 注册一个命令行组
    hogwarts = parser.getgroup("hogwarts")
    # 第一个参数为指定的命令行的参数的形式
    # pytest .\temo.py --browser=chrome
    # 注册一个命令行参数
    hogwarts.addoption("--browser",
                       # 参数默认值
                       default="firefox",
                       # 存储的变量
                       dest="browser",
                       # 参数的描述信息
                       help="指定执行的浏览器")


def pytest_configure(config:Config):
    browser = config.getoption("browser")
    print(f"通过命令行获取到的浏览器为{browser}")
    web_env[browser] = browser
