# -*- coding: UTF-8 -*-
# author: joker
# perject:Decorator.py
# name:test_product_launch.py
# date:2022/10/30
# 全流程实战web
import pytest
from litemall_work.page_add.login_page import LoginPage
from litemall_work.page_list.list_page import ListPage


class TestProductLaunch:

    # 前置处理
    def setup_class(self):
        self.manager = LoginPage()

    def teardown_class(self):
        self.manager.exit()

    # 测试商品上架
    @pytest.mark.run(order=1)
    @pytest.mark.parametrize("product_name", ["joker"])
    def test_product_launch(self, product_name):
        real_name = self.manager.login_in() \
            .go_to_product_launch() \
            .product_launch(product_name) \
            .get_product_name()
        assert real_name == product_name

    # 搜索商品并编辑，更改商品名称
    @pytest.mark.run(order=2)
    @pytest.mark.parametrize("product_name,new_name", [["joker",'joker123']])
    def test_search_product(self, product_name, new_name):
        update = ListPage(self.manager.driver).list(product_name) \
            .editor_name(new_name).get_product_name()
        assert update == new_name

