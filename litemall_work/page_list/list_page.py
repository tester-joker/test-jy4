# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:list_page.py
# date:2022/10/30
from selenium.webdriver.common.by import By
from litemall_work.base.base_page import BasePage


class ListPage(BasePage):
    _INPUT_NAME = (By.XPATH, "//input[contains(@placeholder,'商品名称')]")
    _SEARCH = (By.XPATH, "//*[@class='el-icon-search']/..")
    _EDITOR = (By.XPATH, "//span[text()='编辑']/..")

    def list(self, name):
        # 输入搜索名称
        self.do_send_keys(name, *self._INPUT_NAME)
        # 点击查找按钮
        self.do_click(*self._SEARCH)
        self.do_click(*self._EDITOR)
        from litemall_work.page_list.editor_page import EditorPage
        return EditorPage(self.driver)
