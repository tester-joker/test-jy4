# -*- coding: UTF-8 -*-
# author: joker
# perject:jy4
# name:editor_page.py
# date:2022/10/30
from selenium.webdriver.common.by import By
from litemall_work.base.base_page import BasePage


class EditorPage(BasePage):
    _NEW_NAME = (By.XPATH, "//label[@for='name']/../div/div/input")
    _UPDATE = (By.XPATH, "//span[text()='更新商品']")

    def editor_name(self, name):
        # 清除原本内容
        self.clean(*self._NEW_NAME)
        # 将商品名称更新
        self.do_send_keys(name, *self._NEW_NAME,)
        # 滑动到底部
        self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        # 点击保存修改
        self.do_click(*self._UPDATE)
        from litemall_work.page_add.product_list_page import ProductListPage
        return ProductListPage(self.driver)
