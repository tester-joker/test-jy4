# -*- coding: UTF-8 -*-
# author: joker
# perject:Decorator.py
# name:product_list_page.py
# date:2022/10/30
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from litemall_work.base.base_page import BasePage


class ProductListPage(BasePage):
    _TEXT_PRODUCT_NAME = (By.XPATH, "//tbody/tr[1]/td[3]/div")

    def get_product_name(self):
        # 获取第一个商品名称
        element = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(self._TEXT_PRODUCT_NAME))
        name = element.text
        # 返回第一个商品名称
        return name
