# -*- coding: UTF-8 -*-
# author: joker
# perject:Decorator.py
# name:login_page.py
# date:2022/10/30
from selenium.webdriver.common.by import By

from litemall_work.base.base_page import BasePage


class LoginPage(BasePage):
    _INPUT_USERNAME = (By.XPATH, "//input[@name='username']")
    _INPUT_PASSWORD = (By.XPATH, "//input[@name='password']")
    _BUTTON_LOGIN = (By.CSS_SELECTOR, "button.el-button")

    def login_in(self):
        # 输入用户名
        self.do_send_keys("admin123", *self._INPUT_USERNAME)
        # 输入密码
        self.do_send_keys("admin123", *self._INPUT_PASSWORD)
        # 点击登录按钮
        self.do_click(*self._BUTTON_LOGIN)
        from litemall_work.page_add.home_page import HomePage
        return HomePage(self.driver)
