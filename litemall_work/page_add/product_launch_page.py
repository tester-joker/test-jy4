# -*- coding: UTF-8 -*-
# author: joker
# perject:Decorator.py
# name:product_launch_page.py
# date:2022/10/30
from selenium.webdriver.common.by import By
from litemall_work.base.base_page import BasePage
from litemall_work.page_add.product_list_page import ProductListPage


class ProductLaunchPage(BasePage):
    _INPUT_PRODUCT_CODE = (By.XPATH, "//label[@for='goodsSn']/../div/div/input")
    _INPUT_PRODUCT_NAME = (By.XPATH, "//label[@for='name']/../div/div/input")
    _INPUT_PRODUCT_PRICE = (By.XPATH, "//label[@for='counterPrice']/../div/div/input")
    _RADIO_IS_HOT = (By.XPATH, "//span[contains(text(), '热卖')]")
    _BUTTON_LAUNCH = (By.XPATH, " //div[@class='op-container']//span[contains(text(), '上架')]")

    def product_launch(self, product_name):
        # 输入“商品编号”
        self.do_send_keys(product_name, *self._INPUT_PRODUCT_CODE)
        # 输入“商品名称”
        self.do_send_keys(product_name, *self._INPUT_PRODUCT_NAME)
        # 输入“商品售价”
        self.do_send_keys("888", *self._INPUT_PRODUCT_PRICE)
        # 选择“热卖”
        self.do_click(*self._RADIO_IS_HOT)
        # 滑动到底部
        self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        # 点击“上架”
        self.do_click(*self._BUTTON_LAUNCH)
        # 跳转到商品列表界面
        return ProductListPage(self.driver)
