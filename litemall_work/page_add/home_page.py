# -*- coding: UTF-8 -*-
# author: joker
# perject:Decorator.py
# name:home_page.py
# date:2022/10/30
from selenium.webdriver.common.by import By
from litemall_work.base.base_page import BasePage


class HomePage(BasePage):
    _MENU_PRODUCT_MANAGE = (By.XPATH, "//span[contains(text(), '商品管理')]")
    _MENU_PRODUCT_LAUNCH = (By.XPATH, "//span[contains(text(), '上架')]")

    def go_to_product_launch(self):
        # 点击“商品管理”菜单
        self.do_click(*self._MENU_PRODUCT_MANAGE)
        # 点击“商品上架”
        self.do_click(*self._MENU_PRODUCT_LAUNCH)
        # 进入商品上架页面
        from litemall_work.page_add.product_launch_page import ProductLaunchPage
        return ProductLaunchPage(self.driver)
