# -*- coding: UTF-8 -*-
# author: joker
# perject:Decorator.py
# name:base_page.py
# date:2022/10/30
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By


class BasePage:
    # litemall商城地址
    _BASE_URL = "https://litemall.hogwarts.ceshiren.com/"

    def __init__(self, driver: WebDriver = None):
        if driver is None:
            # 初始化浏览器
            self.driver = webdriver.Chrome()
            self.driver.implicitly_wait(5)
            self.driver.maximize_window()

            # 打开litemall网站
            self.driver.get(self._BASE_URL)
        else:
            # 设置浏览器
            self.driver = driver

    def do_click(self, by: By, locator: str):
        # 点击操作
        self.driver.find_element(by, locator).click()

    def do_send_keys(self, value: str, by: By, locator: str):
        # 输入操作
        element = self.driver.find_element(by, locator)
        element.clear()
        element.send_keys(value)

    def exit(self):
        self.driver.quit()

    def clean(self, by: By, locator: str):
        # 清除操作
        self.driver.find_element(by, locator).clear()
