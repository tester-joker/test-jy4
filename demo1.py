# -*- coding: UTF-8 -*-
'''
# 今有物不知其数，三三数之剩二，五五数之剩三，七七数之余二
def demo1():
    for i in range(1,101):
        if (i % 3 == 2) & (i % 5 == 3) & (i % 7 == 2):
            print(f"{i}符合条件")

# 今有雉兔同笼，上有三十五头，下有九十四足，问雉兔各几何
def demo2():
    # for i in range(36):
    #     for j in range(36):
    #         if i+j==35 and i*2+j*4 == 94:
    #             print(f"鸡有{i}只，兔子有{j}只")

    #列表推导式的写法
    a = [print(f"鸡有{i}只，兔子有{35-i}只") for i in range(36) if i*2+(35-i)*4 == 94]

if __name__ =="__main__":
    demo1()
    demo2()
'''

# 类型注释
# 为类型起别名
# from typing import List
#
# Vector = List[float]
# def scale(scale:float,vector:Vector) -> Vector:
#     return [scale * num for num in vector]
#
# print(scale(1,[1.2,1.3,5.3]))

# class MyException(Exception):
#     def __init__(self,msg):
#         print(f"这是一个异常:{msg}")
#
# def set_age(num):
#     if num<=0 or num>200:
#         raise MyException(f"值错误：{num}")
#     else:
#         print(f"年龄为{num}")
#
# set_age(-1)


# import datetime
# 时间转字符串
# nowtime = datetime.datetime.now()
# r=nowtime.strftime('%a,%b %d %H:%M')
# print(r)

# 字符串转时间
# s='2021-03-05 06:23:04'
# s1 = datetime.datetime.strptime(s,'%Y-%m-%d %H:%M:%S')
# print(s1)

# 编写一段代码，生成以时间命名的日志文件。并向文件中写入日志信息
# import datetime
# now = datetime.datetime.now()
# now_time = now.strftime('%Y-%m-%d')
# print(now_time)
#
# with open(now_time,'w+',encoding='utf-8') as f:
#     f.write(f'{now} [info] line:14 这是一个日志信息')


# import json
# data = {
#     'a':1,
#     'b':[12,34,5],
#     'c':True
# }
# 将python对象编码为json字符串
# json_data = json.dumps(data)
# print(json_data)

# 将json字符串转成python对象
# json_data = '{"a": 1, "b": [12, 34, 5], "c": true}'
# python_data = json.loads(json_data)
# print(python_data)
# print(type(python_data))
# import log
# import threading
# import time
#
# def tesk(i):
#     time.sleep(3)
#     log.info(time.time())
#
# def main():
#     log.basicConfig(level=log.INFO)
#     thread1 = threading.Thread(target=tesk,args=(1,))
#     thread2 = threading.Thread(target=tesk,args=(2,))
#     thread1.start()
#     thread2.start()
#     print(time.time())
#     log.info("主程序")
#
# if __name__ == '__main__':
#     main()


# import yaml
#
# data = {
#     "client":{"default":"utf-8"},
#     "mysql":{"user":"root","password":123},
#     "custom":{
#         "user1":{"user":"张三","pwd":666},
#         "user2":{"user":"李四","pwd":999},
#     }
# }
# #用dump方法将python对象转成yaml格式的文件
# with open('./my.yaml','w',encoding='utf-8') as f:
#     # 如果写入的对象含有中文，需要设置一个参数，allow_unicode为True，这样就可以正常显示中文了
#     yaml.dump(data,f,allow_unicode=True)


# 读取yaml文件中的内容转化成python对象
# import yaml
# with open('./my.yaml','r',encoding='utf-8') as f:
#     #解析流中的第一个YAML文档并生成相应的Python对象。
#     data = yaml.safe_load(f)
# print(data)
# print(type(data))


# class Stu:
#     def __init__(self,age):
#         self.__age = age
#     @property
#     def age(self):
#         return self.__age
#
#     @age.setter
#     def age(self,age:int):
#         if age > 100 or age < 0:
#             print("不合法")
#         else:
#             self.__age = age
#             return self.__age
#
# if __name__ == '__main__':
#     s = Stu(10)
#     # 修改前
#     print(s.age)
#     s.age = -1
#     print(s.age)

# import pytest
# 1.参数化的名字，要与方法中的参数名一一对应
# 2. 如果传递多个参数的话，要放在列表中，列表中可以嵌套列表/元组
# 3. 可以通过ids参数设定测试用例名称，而且ids设置的个数要与传递数据个数一致
# @pytest.mark.parametrize("test_input,expected",[
#     ("3+5",8),
#     ("6+6",12),
#     ("2+3",5)
# ],ids=["num1","num2",'num3'])
# def test_mark(test_input,expected):
#     assert eval(test_input) == expected

# a = True
# @pytest.mark.skipif(a,reason="跳过")
# def test_a():
#     print(a)

# @pytest.mark.xfail(reason="bug")
# def test_xfail():
#     print("1234")
#     assert 1 == 2


# import pytest
# import yaml
#
# class TestDemo:
#     @pytest.mark.parametrize("env",yaml.safe_load(open("./env.yml")))
#     def test_demo(self,env):
#         if "test" in env:
#             print("这是测试环境")
#             print(env['test'])
#         elif "dev" in env:
#             print("这是开发环境")


# import openpyxl
#
# book = openpyxl.load_workbook('params.xlsx')
# # 获取工作簿
# sheet = book.active
# # 获取单元格数据
# a1 = sheet['A1'].value
# print(a1)
# c3 = sheet.cell(column=3,row=3).value
# print(c3)
#
# # 获取多个单元格，转成元组对象
# cells = sheet['A1':'C3']
# print(cells)


# import allure
# class TestDemo():
#     @allure.story("登录成功")
#     def test_allure(self):
#         with allure.step("步骤1：打开应用"):
#             print("打开应用")
#         with allure.step("步骤2：登录"):
#             print("登录界面")
#         with allure.step("步骤3：输入用户信息"):
#             print("这是登录界面")


# def solution(s: str) -> list:
#     if len(s)%2 == 0:
#         s = list(s)
#         for i in range(0,len(s)-1,2):
#             s[i] += s[i+1]
#         return s[::2]
#     elif len(s) == 0:
#         return []
#     else:
#         s = s+'_'
#         return solution(s)
#
# assert solution("asdfadsf") == ['as', 'df', 'ad', 'sf']
# assert solution("asdfads") == ['as', 'df', 'ad', 's_']
# assert solution("awsdwerd") == ['aw', 'sd', 'we', 'rd']
# assert solution("") == []
# assert solution("x") == ['x_']
# assert solution("xy") == ['xy']

# 二分查找
# def demo(nums:list,target:int):
#     first=0
#     last=len(nums)-1
#     while first<=last:
#         mid = (last+first) // 2
#         if target > nums[mid]:
#             first = mid+1
#         elif target < nums[mid]:
#             last = mid-1
#         else:
#             return mid
#     return -1
#
# print(demo([-1,0,3,4,6,10,13,14],14))

import time


# 时间的打印
# t = int(time.time())
# timeArray = time.localtime(t)
# print(time.strftime("%Y-%m-%d %H:%M:%S", timeArray))


# class Demo:
#     demo = 'test'
#     _username = 'hogwarts'
#     __password = '123'
#
#     @property
#     def password(self):
#         return self.__password
#
#     @password.setter
#     def password(self,value):
#         # 进行校验
#         if len(value) >= 6:
#             self.__password = value
#         else:
#             print("密码长度要大于六位！")


# re = Demo()
# print(re.password)

# print(Demo.demo)
# print(Demo._username)
# python内置的特殊属性，是一个字典，里面包含所有可写的属性
# print(Demo.__dict__)

# obj = Demo()
# 修改私有属性的值（满足校验）
# obj.password = 'hogwarts214'
# print(obj.password)
# 修改私有属性的值（不满足校验）
# obj.password = 'h214'
# print(obj.password)



